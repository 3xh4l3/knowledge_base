---
title: Bind9
---

# Bind

## Настройка логирования

```
logging {
    channel bind.log {
        file "/var/lib/bind/bind.log" versions 10 size 20m;
        severity warning;
        print-category yes;
        print-severity yes;
        print-time yes;
    };
  
        category queries { bind.log; };
        category default { bind.log; };
        category config { bind.log; };
};
```

### Troubleshoot

#### Ошибка

Jan 23 17:33:31 abk-ns2 kernel: [3724840.299892] audit: type=1400 audit(1579775611.622:12809): apparmor="DENIED" operation="link" profile="/usr/sbin/named" name="/etc/bind/zones/slave/db-td8wW4DB" pid=370 comm="isc-worker0003" requested_mask="l" denied_mask="l" fsuid=106 ouid=106 target="/etc/bind/zones/slave/db.acs-eltex.local0..hosts"

#### Решение

**/etc/apparmor.d/usr.sbin.named**

```
...
  # /etc/bind should be read-only for bind
  # /var/lib/bind is for dynamically updated zone (and journal) files.
  # /var/cache/bind is for slave/stub data, since we're not the origin of it.
  # See /usr/share/doc/bind9/README.Debian.gz
  /etc/bind/** rw,
  /etc/bind/zones/slave/** lrw,
  /etc/bind/zones/slave/ rw,
  /var/lib/bind/** rw,
  /var/lib/bind/ rw,
  /var/cache/bind/** lrw,
  /var/cache/bind/ rw,
...
```

#### Ошибка

Не трансферятся зоны на слейв
23-Jan-2020 22:34:36.773 xfer-in: error: transfer of 'abk.orionnet.ru/IN' from 193.106.168.14#53: failed to connect: connection refused
23-Jan-2020 22:46:52.573 xfer-in: error: transfer of '168.106.193.in-addr.arpa/IN' from 193.106.168.14#53: failed to connect: connection refused

#### Решение

Проверить, слушает ли `bind` TCP Port 53. Если нет, до добавить директиву в конфиг бинда
`listen-on { any; };`