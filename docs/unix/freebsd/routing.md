---
title: Routing
---

# Routing

Для всех IP адресов на интерфейсах создается хост маршрут в `lo0`. Например:

```bash
# адрес на интерфейсе 172.28.95.254/24
> ifconfig vlan419 inet
vlan419: flags=29943<UP,BROADCAST,RUNNING,PROMISC,SIMPLEX,LINK0,MULTICAST,PPROMISC> metric 0 mtu 1500
        options=80000<LINKSTATE>
        inet 172.28.95.254 netmask 0xfffff000 broadcast 172.28.95.255

# host маршрут
> route -n get 172.28.95.254
   route to: 172.28.95.254
destination: 172.28.95.254
        fib: 0
  interface: lo0
      flags: <UP,HOST,DONE,STATIC,PINNED>
 recvpipe  sendpipe  ssthresh  rtt,msec    mtu        weight    expire
       0         0         0         0     16384         1         0 

# connected маршрут
> route -n get 172.28.95.255
   route to: 172.28.95.255
destination: 172.28.80.0
       mask: 255.255.240.0
        fib: 0
  interface: vlan419
      flags: <UP,DONE,PINNED>
 recvpipe  sendpipe  ssthresh  rtt,msec    mtu        weight    expire
       0         0         0         0      1500         1         0 
       
# в netstat это будет выглядет так
netstat -rn4F0 | grep 172.28.95.254
172.28.95.254      link#3             UHS         lo0
```

Создание, удаление маршрутов можно отследить. Туда же попадают и добавление/удаление NDP и ARP записей.

```bash
route -n monitor
```

