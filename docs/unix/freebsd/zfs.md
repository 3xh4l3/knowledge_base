---
title: ZFS
---

# ZFS

## Шпаргалки

```bash
# Отцепляем и чистим старый диск
smartctl -a /dev/adaX
gpart show -l
gpart backup adaX adaX.backup
swapinfo 
swapoff /dev/gpt/swapX
zpool status
zpool detach zroot gpt/rootX
newfs -E -t -b 64k -f 64k -i 64M /dev/adaX
gpart destroy adaX

# Втыкаем новый диск
newfs -E -t -b 64k -f 64k -i 64M /dev/adaX
gpart destory -F adaX
gpart restore -l adaX < adaX.backup
# Ну или разметить новый диск вручную
# Далее цепляем диск в пул к уже суещсвующему, где Y-существующий, Z-новый
zpool attach zroot /dev/gpt/rootY /dev/gpt/rootZ
swapon /dev/gpt/swap1
```

## ARC

## Compress

