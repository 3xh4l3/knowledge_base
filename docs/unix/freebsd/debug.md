---
title: Debug
---

# Debug

Если провалились в дебаггер, то сразу можно собрать `core` файл

```bash
gdb>dump
```

Чтобы корка собиралась автоматически в `rc.conf` должны быть следующие строчки:

```bash
# AUTO - savecore сам определит, с какого swap раздела прочитать дамп
dumpdev="AUTO"
# можно указать явным образом

dumpdir="/var/crash"
savecore_enable="YES"
```

Чтобы сервер при панике автоматически перезагружался, а не вываливался в дебаггер, нужно добавить в `susctl.conf` следующую стрчоку:

```bash
# Автоматическая перезагрузка при панике
debug.debugger_on_panic=0
```

Если сервер завис и никак не реагирует, можно попробовать насильно свалить его в дебаггер (interrupt-to-debugger) кобминацией клавиш `Ctrl`+`Alt`+`ESC`.