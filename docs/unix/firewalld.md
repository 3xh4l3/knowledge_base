---
title: Firewalld
---

# Firewalld

```bash
# Управление
systemctl start|stop|enable|disable|status firewalld
# или
firewall-cmd --state
# Безопасная перезагрузка без потери соединений
firewall-cmd --reload
# Перезагрузка со сбросом всех соединений
firewall-cmd --complete-reload

# Получение информации о зонах
firewall-cmd --get-zones
firewall-cmd --list-all
firewall-cmd --list-all --permanent
firewall-cmd --get-default-zone
firewall-cmd --get-active-zones
firewall-cmd --get-zone-of-interface=eth0
firewall-cmd --zone=public --list-interfaces

# Информация о сервисах
firewall-cmd --get-services
firewall-cmd --zone=public --list-service

# Информация о портах
firewall-cmd --list-ports

# Отключение firewalld
systemctl disable firewalld
systemctl stop firewalld
systemctl mask firewalld 
systemctl status firewalld
# Совесем удалить
yum remove firewalld
```

## Настройки зон по-умолчанию

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-using_firewalls#sec-Choosing_a_Network_Zone

- **drop** - все входящие пакеты отбрасываются (drop) без ответа. Разрешены только исходящие соединения.
- **block** - входящие соединения отклоняются (rejected) с ответом icmp-host-prohibited (или icmp6-adm-prohibited). Разрешены только инициированные системой соединения.
- **public** - зона по-умолчанию. Из названия ясно, что эта зона нацелена на работу в общественных сетях. Мы не доверяем этой сети и разрешаем только определенные входящие соединения.
- **external** - зона для внешнего интерфейса роутера (т.н. маскарадинг). Разрешены только определенные нами входящие соединения.
- **dmz** - зона DMZ, разрешены только определенные входящие соединения.
- **work** - зона рабочей сети. Мы все еще не доверяем никому, но уже не так сильно, как раньше :) Разрешены только определенные входящие соединения.
- **home** - домашняя зона. Мы доверяем окружению, но разрешены только определенные входящие соединения
- **internal** - внутренняя зона. Мы доверяем окружению, но разрешены только определенные входящие соединения
- **trusted** - разрешено все

```bash
# Добавить/удалить интерфейс в зону перманентно
firewall-cmd --zone=public --change-interface=eth1 --permanent

# Перманентно добавить сервис в нужную зону
firewall-cmd --zone=public --add-service=samba --add-service=samba-client --permanent

# Перманентно удалить разрешенный сервайс
firewall-cmd --zone=public --remove-service=dhcpv6-client --permanent
firewall-cmd --permanent --zone=public --remove-service=ssh

# Добавить порт в зону
firewall-cmd --zone=public --add-port=5000/tcp
firewall-cmd --permanent --zone=public --add-port=1-22/tcp

# Добавить в зону доступ с указанных адресов
firewall-cmd --permanent --zone=public --add-source=192.168.100.0/24
firewall-cmd --permanent --zone=public --add-source=192.168.222.123/32
```

## Еще команды

```bash
# Блокировать все входящие
firewall-cmd --panic-on
firewall-cmd --panic-off
firewall-cmd --query-panic

# Сделать все текущие настройки перманентными
firewall-cmd --runtime-to-permanent

# Маскарадинг
firewall-cmd --zone=external --query-masquerade
firewall-cmd --zone=external --add-masquerade
firewall-cmd --zone=external --add-forward-port=port=22:proto=tcp:toaddr=192.168.1.23
firewall-cmd --zone=external --add-forward-port=port=22:proto=tcp:toport=2055:toaddr=192.168.1.23
```

## Примеры

Разрешить доступ только с указанных IP адресов на указанные порты

```bash
firewall-cmd --permanent --zone=work --add-source=191.206.170.2
firewall-cmd --permanent --zone=work --add-source=89.15.16.5
firewall-cmd --permanent --zone=work --add-source=89.15.16.7
#firewall-cmd --permanent --zone=work --add-port=8080/tcp
firewall-cmd --permanent --zone=work --add-service=ssh
firewall-cmd --permanent --zone=work --add-service=http
firewall-cmd --permanent --zone=work --add-service=https
firewall-cmd --set-default-zone=drop
firewall-cmd --reload

# qoestor
firewall-cmd --permanent --zone=work --add-source=191.206.168.52
firewall-cmd --permanent --zone=work --add-source=191.206.168.53
firewall-cmd --permanent --zone=work --add-source=191.206.168.49
firewall-cmd --permanent --zone=work --add-source=191.206.170.2
firewall-cmd --permanent --zone=work --add-source=191.206.171.132
firewall-cmd --permanent --zone=work --add-port=1500/tcp
firewall-cmd --permanent --zone=work --add-port=1510/tcp
firewall-cmd --permanent --zone=work --add-port=1501/tcp
firewall-cmd --permanent --zone=work --add-port=1511/tcp
firewall-cmd --permanent --zone=work --add-port=8123/tcp
firewall-cmd --permanent --zone=work --add-service=ssh
firewall-cmd --permanent --zone=work --remove-service=dhcpv6-client
firewall-cmd --set-default-zone=drop
firewall-cmd --reload
```

