---
title: Rsync
---


title: Rsync
--

`Rsync` умеет показывать, что собирается делать, а так же умеет копировать только новые и изменившиеся файлы.

Нюансы
* ==/== в конце - скопируется содержимое каталога
* без ==/== в конце - скопируется сам каталог со всем содержимым

Нужные ключики
```
-a - рекурсивное копирование файлов и каталогов, включая их атрибуты (дата создания, изменения). Аналогичен -rlptgoD.
-n - тестовый прогон
-v - подробный вывод
-P - показывает прогресс и продолжает прерванное копирование. Аналогичен --progress --partial.
-W - копировать изменившиейся файлы целиком, чтобы не тратить ресурсы на вычисление изменившихся кусков файла.
-С - не копировать файлы всяких систем контроля версий (git, svn и т.д.)
-F - использовать файл исключений .rsync-filter при копировании.
--delete - полная синхронизация с учетом удаленных файлов.
--delete-excluded - по факту удалить исключенные файлы уже после копирования.
```

Исключаем файлы из копирования. Для этого создаем файлик `.rsync-filter` в любой поддиректории копируемого каталога. Пути указываются относительно той директории, где лежит файл.
```
# не будет скопирован каталог или файл .cache, но будут скопированы foo/.cache и foo/bar/.cache
- /.cache
# не будет скопирован каталог
- /.cache/
# то же самое в любой поддиректории
- /**/.cache
- /**/.cache/
```

Пример конфига
```
- /**/.git
- /**/lib/python**
+ /Videos**
+ /GNS3**
+ /FTP**
+ /TFTP**
+ /openvpn-ca**
+ /Pictures**
+ /Servers**
+ /Soft**
+ /Work**
+ /VIRT**
+ /Documets**
+ /Downloads**
+ /Dev**
+ /Desktop
+ /cisco-videos
+ /.bashrc
+ /.bash_aliases
+ /.bash_history
+ /.bash_logout
+ /.vimrc
+ /bin**
+ /.ssh**
+ /.rsync-filter
+ /.gitconfig
+ /.thunderbird**
+ /.docker**
+ /.remmina**
+ /.vnc**
+ /.vscode/**
+ /.config/bacula.org**
+ /.config/draw.io**
+ /.config/GNS3**
+ /.config/hexchat**
+ /.config/keepassx**
+ /.config/keepassxc**
+ /.config/lxc**
+ /.config/nvim**
+ /.config/ranger**
+ /.config/Typora**
- **
```

Копирование файлов:
```bash
rsync -aFCP --delete --delete-excluded /home/username /media/username/MIGRATION/
```
