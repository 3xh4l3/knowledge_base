---
title: Proxmox
---

# Proxmox

Убрать назойливое сообщение об отсутствии подписки

```
sed -i.bak "s/data.status !== 'Active'/false/g" /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js && systemctl restart pveproxy.service
```

