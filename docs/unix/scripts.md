---
title: Скрипты
---

# Скрипты

Создать swap в файле_

```bash
fallocate -l 2G /swapfile
dd if=/dev/zero of=/swapfile bs=1024 count=2097152
chmod 600 /swapfile
/usr/sbin/mkswap /swapfile
/usr/sbin/swapon /swapfile
echo '/swapfile swap swap defaults 0 0' >> /etc/fstab
/usr/sbin/swapon --show
```

Показать утилизаторов памяти

```bash
 ps aux  | awk '{print $6/1024 " MB\t\t" $11}'  | sort -n
```

Показать утилизаторов swap-а

```bash
grep VmSwap /proc/*/status 2>/dev/null | sort -nk2 | tail -n5
```

Очистить кеш и swap

```bash
echo 3 > /proc/sys/vm/drop_caches && swapoff -a && swapon -a
```

