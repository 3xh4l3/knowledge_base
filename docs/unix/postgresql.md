---
title: PostgreSQL
---

# PostgreSQL

### Использование

Войти в консоль. Необходимо выполнить `psql` от пользователя `postgres`

```bash
sudo -u postgres psql
```

### Команды

Помощь по командам

```sql
\s
```

Список пользователей

```sql
\du+
```

Список БД

```\
\l+
```

Подключиться к БД

```
\c <db_name>
```



Сменить пароль пользователю

```sql
# sudo -u user_name psql db_name
ALTER USER user_name WITH PASSWORD 'new_password';
```



### Резервное копирование

Простейший вариант. Выгрузка в sql файл

```bash
pg_dump netbox > netbox.sql
```

Со сжатием. Наилучший вариант.

```bash
# Дамп
pg_dump -F c -f /root/backups/netbox_db/netbox-`date +%F`.tar.gz netbox
# Восстановление
pg_restore -F c -d netbox /root/backups/netbox_db/netbox-2020-10-04.tar.gz
```