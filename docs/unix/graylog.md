---
title: Graylog
---

# Graylog

## Проблемы

Input syslog не понимает сообщения, которые шлет `rsyslogd`. Необходимо настраивать rsyslogd следующим образом

```bash
local7.*      @100.100.100.100:1515;RSYSLOG_SyslogProtocol23Format
&~
```

