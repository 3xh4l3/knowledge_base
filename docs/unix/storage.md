---
title: "Хранилки"
---

Все о файловых системах, хранилищах и сетевых протоколах, связанных с хранением данных.

## Запись носителей

Пишем образ на флешку для установки

```bash
sudo dd if=CentOS-7-x86_64-NetInstall-2003.iso of=/dev/sdb bs=512k status=progress
```

## Разбивка

Посмотреть, какие диски видит система

```bash
fdisk -l
# или
lsblk
```

Создаем файловую систему

```bash
# Классическим способом
fdisk /dev/sdb
...

# Если диск больше 2Тб
parted
(parted) mklabel gpt
(parted) mkpart primary 0GB 20000GB
# Далее создаем файловую систему в разделе
mkfs.ext4 /dev/sdb1
```
