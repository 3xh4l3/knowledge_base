---
title: Elasticsearch
---

## Управление

Эластик управляется через API по HTTP(S) на порту 9002. То есть команды на указанный порт можно слать любым HTTP(S) клиентом, например Curl-ом.

## Полезные команды

Если на диске кончилось место, индексы переключаются в *readonly*. Чтобы их вернуть можно воспользоваться следующией командой:

```bash
curl -XPUT -H "Content-Type: application/json"  http://localhost:9200/_all/_settings -d '{"index.blocks.read_only_allow_delete": null}'
```

