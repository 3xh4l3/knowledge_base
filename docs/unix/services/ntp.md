---
title: NTP
---

# NTP

## Debian 10

```bash
timedatectl list-timezones
timedatectl set-timezone Asia/Krasnoyarsk
apt install ntp
systemctl enable ntp
systemctl is-enabled ntp
```

## Centos 8

```bash
yum install chrony
systemctl start chronyd 
systemctl enable chronyd

timedatectl status
```

Конфиг этого дела лежит в `/etc/chrony.conf`

## Ссылки

https://selectel.ru/blog/nastrojka-ntp-na-servere/