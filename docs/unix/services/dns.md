---
title: DNS
---

# DNS

## bind9

```bash
# Очистить кеш
rndc flush
```

## Инстументы

#### DIG

Посмотреть TTL у домена (второй столбец). Запрос к неавторитетному серверу вернет оставшееся время до обновления записи.

```bash
# Оставшееся время жизни в кеше нашего сервера по-умолчанию
$dig  +nocmd +noall +answer yandex.ru
yandex.ru.		64	IN	A	77.88.55.70
yandex.ru.		64	IN	A	77.88.55.77
yandex.ru.		64	IN	A	5.255.255.70
yandex.ru.		64	IN	A	5.255.255.77
# Заданное время жизни на авторитетном сервере
dig  +nocmd +noall +answer yandex.ru @ns1.yandex.ru.
yandex.ru.		300	IN	A	77.88.55.77
yandex.ru.		300	IN	A	77.88.55.70
yandex.ru.		300	IN	A	5.255.255.70
yandex.ru.		300	IN	A	5.255.255.77
```
