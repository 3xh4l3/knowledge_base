---
title: Nginx
---

# Nginx

## HTTPS

```c
server {
  listen 443 ssl;
  server_name example.com;

  client_max_body_size 2M;

  server_name url.example.com;

  ssl_certificate     /root/ssl/bundle.crt;
  ssl_certificate_key /root/ssl/privatekey.pem;

  root /usr/share/nginx/examplesite;

  location / {
    try_files $uri $uri/ /ui /ui/$uri =404;
    index /ui/index.html;
    auth_basic "Restricted access!";
    auth_basic_user_file /etc/nginx/.htpasswd;
  }

  location /openapi.json {
      try_files $uri @proxy_to_app;
  }

  location /custom/ {
      try_files $uri $uri/ /custom;
  }

  location /images/ {
      try_files $uri $uri/ /images;
  }

  location /api {
      try_files $uri @proxy_to_app;
  }

  location @proxy_to_app {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header Host $http_host;
    proxy_redirect off;
    proxy_pass http://servicehost:8001;
  }

}

```