---
title: sed
---

**Добавить в файл строчку**
Строчка вставится для каждого совпадения

```bash
# После совпадения
sed -i '/pattern/ a new_string' file.txt
# Перед совпадением
sed -i '/pattern/ i new_string' file.txt
```