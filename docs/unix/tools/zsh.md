---
title: ZSH
---

# ZSH

## Настройка

Интерактивная первоначальная настройка

```bash
autoload -Uz zsh-newuser-install
zsh-newuser-install -f
```



## Автодополнение

**compsys** - отвечает за автодополнение (*man zshcompsys*).

Сконфигурировать можно командой:

```bash
autoload -Uz compinstall
compinstall
```

FreeBSD

```bash
/usr/local/share/zsh/5.8/functions/Completion/*
```

