---
title: LXC(D)
---

# LXC and LXD

Переимнование
```bash
lxc stop <name>
lxc move <name> <newname>
```
Автозагрузка
```bash
lxc config set {vm-name} {key} {value}
lxc config set {vm-name} boot.autostart {true|false}
lxc config set {vm-name} boot.autostart.priority integer
lxc config set {vm-name} boot.autostart.delay integer
```
Монтирование папки в хост систему
```bash
lxc stop <containter_name>
# добавляем устройство внутрь контейнера 
lxc config device add <container_name> <device_name> disk source=<host_folder> path=<container_folder>
# both - uid/gid, 1000 - пользователь на хост машние, 0 - root  в контейнере, либо любой другой пользователь
lxc config set <container_name> raw.idmap "both 1000 0"
```
Создание контейнера
```bash
# Список доступных темплейтов
lxc image list images: 
# Запустить 
lxc launch images:debian/10 zabbix-bot
```

