---
title: Nftables
---

# Nftables

Акутально с версии Debian 10

```
apt install nftables
cat /etc/nftables.conf

systemctl enable nftables
systemctl start nftables
systemctl status nftables
```



### Управление

Текущий список правил
`nft list ruleset`
Очистить все цепочки
`nft flush ruleset`
Выгрузить правила в автозагрузку

```bash
echo '#!/usr/sbin/nft -f' > /etc/nftables.conf
echo 'flush ruleset' >> /etc/nftables.conf
nft list ruleset >> /etc/nftables.conf
```

Загрузить из файла

```bash
nft -f /etc/nftables.conf
```

Примеры правил

```
nft add set inet filter orion { type ipv4_addr\; }
nft add element inet filter orion { 191.206.170.2 }
nft delete element inet filter orion { 191.206.170.2 }
nft insert rule inet filter input position 5 ip saddr @orion accept
```

Пример конфига

```
#!/usr/sbin/nft -f

flush ruleset

table inet filter {
	set orion {
		type ipv4_addr
		elements = { 8.65.1.5, 8.65.1.7,
			     8.65.1.93, 191.206.170.2,
			     191.206.171.132, 191.206.171.134 }
	}

	chain input {
		type filter hook input priority 0; policy accept;
		ip saddr @orion accept
		tcp dport http drop
        tcp dport https drop
        tcp dport ssh drop
	}

	chain forward {
		type filter hook forward priority 0; policy accept;
	}

	chain output {
		type filter hook output priority 0; policy accept;
	}
}
```
