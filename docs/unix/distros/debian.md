---
title: Debian
---

### Скрипты

*Сгенерить русскую локаль*

```bash
apt-get clean && apt-get update && apt-get upgrade -y && apt-get dist-upgrade -y
sed -i '/ru_RU.UTF-8/s/^#//g' /etc/locale.gen
/usr/sbin/locale-gen
```



## Архив репозиториев для старых версий

### Debian 8 jessie

Репозитории Debian 8 jessie:

```
deb http://archive.debian.org/debian/ jessie main non-free contrib
deb-src http://archive.debian.org/debian/ jessie main non-free contrib
deb http://archive.debian.org/debian-security/ jessie/updates main contrib
deb-src http://archive.debian.org/debian-security/ jessie/updates main contrib
```

### Debian 7 wheezy

Репозитории Debian 7 wheezy:

```
deb http://archive.debian.org/debian/ wheezy main non-free contrib
deb-src http://archive.debian.org/debian/ wheezy main non-free contrib
deb http://archive.debian.org/debian-security/ wheezy/updates main contrib
deb-src http://archive.debian.org/debian-security/ wheezy/updates main contrib
```

### Debian 6 squeeze

Репозитории Debian 6 squeeze:

```
deb http://archive.debian.org/debian/ squeeze main non-free contrib
deb-src http://archive.debian.org/debian/ squeeze main non-free contrib
deb http://archive.debian.org/debian-security/ squeeze/updates main contrib
deb-src http://archive.debian.org/debian-security/ squeeze/updates main contrib
```