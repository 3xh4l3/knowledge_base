---
title: CentOS
---

## Настройка системы

### Локаль
```bash
cat /etc/locale.conf
localectl status
localectl list-locales
yum install glibc-langpack-ru.x86_64
localedef ru_RU.UTF-8 -i ru_RU -f UTF-8
localectl set-locale LANG=ru_RU.UTF-8
```

### Время
```bash
rm /etc/localtime
ln -s /usr/share/zoneinfo/Asia/Krasnoyarsk /etc/localtime
```

### Сеть

Собираем **bond** в режиме *active-backup*

Создаем файлик `/etc/sysconfig/network-scripts/ifcfg-bond0`

```bash
TYPE=Bond
DEVICE=bond0
NAME=bond0
BONDING_MASTER=yes
BONDING_OPTS="mode=1 miimon=100"
BOOTPROTO=none
ONBOOT=yes
IPV6INIT=no
USERCTL=no
NM_CONTROLLED=no
IPADDR=192.168.1.10
PREFIX=27
GATEWAY=192.168.1.1
DNS1=192.168.1.1
DNS2=192.168.1.2

```

Конфигурим слейвы

```bash
# ifcfg-enp7s0f0
TYPE=Ethernet
DEVICE=enp7s0f0
NAME=enp7s0f0
BOOTPROTO=none
UUID=e0b52eb4-f2b2-45d6-bb65-0da46f3765d4
ONBOOT=yes
MASTER=bond0
SLAVE=yes
NM_CONTROLLED=no
USERCTL=no

# ifcfg-enp7s0f1
TYPE=Ethernet
DEVICE=enp7s0f0
NAME=enp7s0f0
BOOTPROTO=none
UUID=e0b52eb4-f2b2-45d6-bb65-0da46f3765d4
ONBOOT=yes
MASTER=bond0
SLAVE=yes
NM_CONTROLLED=no
USERCTL=no
```

Посмотреть статус интерфейса `bond0`:

```bash
Ethernet Channel Bonding Driver: v3.7.1 (April 27, 2011)

Bonding Mode: fault-tolerance (active-backup)
Primary Slave: None
Currently Active Slave: enp7s0f0
MII Status: up
MII Polling Interval (ms): 100
Up Delay (ms): 0
Down Delay (ms): 0

Slave Interface: enp7s0f0
MII Status: up
Speed: 1000 Mbps
Duplex: full
Link Failure Count: 1
Permanent HW addr: 1c:c1:de:72:d9:46
Slave queue ID: 0

Slave Interface: enp7s0f1
MII Status: up
Speed: 1000 Mbps
Duplex: full
Link Failure Count: 1
Permanent HW addr: 1c:c1:de:72:d9:47
Slave queue ID: 0
```

## Работа с пакетами

```bash
# Все команды есть в помощи
yum help
# Проверить обновления
yum check-update
# Очистить локальный кеш
yum clean all

# Найти пакет, который содержит нужный бинарник
yum provides /usr/sbin/semanage
# или
yum whatprovides /usr/sbin/semanage
```

## Файрвол

Удаляем `firewalld`

```bash
# Отключение firewalld
systemctl disable firewalld
systemctl stop firewalld
systemctl mask firewalld 
systemctl status firewalld
# Совесем удалить
yum remove firewalld
```

Устанавливаем утилиты для работы с файрволом

```bash
yum -y install iptables-services
systemctl enable iptables.service
systemctl start iptables.service
```

После этого правила можно добавлять в файл `/etc/sysconfig/iptables`, а так же дополнительно можно настроить в файле `/etc/sysconfig/iptables-config` на автоматическое сохранениие правил файл и прочее.
