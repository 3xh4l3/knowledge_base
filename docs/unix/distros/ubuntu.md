---
title: Ubuntu
---

# Ubuntu

## DNS

Начиная с версии 16.01 в Ubuntu используется кеширующий резолвер `systemd-resolved`.

### Очистить кеша DNS-а

```bash
sudo systemd-resolve --statistics
sudo systemd-resolve --flush-caches
sudo systemctl restart systemd-resolved
sudo systemd-resolve --statistics
```

### Отключение systemd-resolved

Этот метод работает на версиях 17.04 (Zesty), 17.10 (Artful), 18.04 (Bionic), 18.10 (Cosmic), 19.04 (Disco) and 20.04 (Focal)

```bash
sudo systemctl disable systemd-resolved
sudo systemctl stop systemd-resolved
```

В конфиг NetworkManager-а добавляем в секцию `[main]` 

`/etc/NetworkManager/NetworkManager.conf`:

```
dns=default
```

Удаляем симлинк `/etc/resolv.conf`

```
rm /etc/resolv.conf
```

Рестартуем NetworkManager

```
sudo systemctl restart NetworkManager
```