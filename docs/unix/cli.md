---
title: CLI
---

# CLI

Повторить предыдущую команду

```bash
!!
```

Например повторить команду, но уже с sudo

```bash
sudo !!
```

Напечатать статус выполнения последней команды
echo `$?`

```bash
0 - success
1 - failed
```

### Запуск процессов в фоне

Все сообщения будут выводится в консоль. 
Команда будет завершена при закрытии терминала.

```bash
command &
```

Открепить команду от текущего пользователя после запуска в фоне.
Команда продолжит выполняться после закрытия терминала.

```bash
command &
disown
```

Запустить команду в фоне с перенаправлением вывода в /dev/null, чтобы в консоль ничего не выводилось

```bash
command &>/dev/null &
```

Запустить команду с запретом завершения (сигнала HUP) при закрытии терминала

```bash
nohup command &>/dev/null &
```



## Полезные команды

```
# Вывести файл без комментариев и пустых строк
... | grep -v '^\s*$\|^\s*\#'
```





