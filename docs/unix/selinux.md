---
title: SELinux
---

# SELinux

Для настройки будем использовать утилиту `semanage`. Если нет утилиты `semanage`, то нужно установить пакет `policycoreutils-python`

```bash
# Список известных портов
semanage port -l

# Разрешить порт для известного сервиса
# Пример: разрешить SSH слушать порт 22022
semanage port -a -t ssh_port_t -p tcp 22022
```

