---
title: Vim
---

### Минимальные настройки

*.vimrc*

```
" Tabs settings
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab

" Autoindent
set autoindent
filetype indent on 

" Highlight
syntax on
let python_highlight_all = 1

" Look
set number
colorscheme blue
set t_ut=""
set t_Co=256
set termencoding=utf-8
set novisualbell " Don't blink
set t_vb= "Don't beep
set ruler
set hidden

" Before write del all traling spaces in py files
autocmd BufWritePre *.py normal m`:%/\s\+$//e ``
" Smart indents for py files
autocmd BufRead *.py set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class

```

### Commands

_:tabnew_ - новая пустая вкладка

