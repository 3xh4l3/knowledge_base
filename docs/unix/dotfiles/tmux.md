---
title: .tmux.conf
---

```bash
set -g mouse on
setw -g mode-keys vi

# Prevent Esc delay in vim
set -g escape-time 5

# Zoom pane
bind -n C-Space resize-pane -Z

# Reload config within tmux
bind C-R source-file ~/.tmux.conf

# Switch panes without prefix Alt+<key>
bind -n M-h select-pane -L
bind -n M-l select-pane -R
bind -n M-k select-pane -U
bind -n M-j select-pane -D

# Toggle pane synchronization
bind-key s set-window-option synchronize-panes\; display-message "synchronize-panes is now #{?pane_synchronized,on,off}"
```

