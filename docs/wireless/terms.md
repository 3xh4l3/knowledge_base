---
title: "Термины"
---

*  **АFC** - *Automatic Frequency Coordinator* - 
* **FFT** - *Fast Fourier Transform* - алгоритм ускоренного вычисления дискретного преобразования Фурье.
* **CSMA/CA** - Collision Avoidance
* **SNR** - 
* **SSID** - 
* **Hidden SSID** - 
* **Beamforming** -
* **OFDM** - *Orthogonal Frequency-Division Multiplexing* - мультиплексирование с ортогональным частотным разделением каналов.
* **OFDMA** -
* **LBT (Listen Before Talk)** - 
* **BSS** - *Basic Service Set* -  взаимодействие узлов через одну AP
* **BSS Coloring** - решение проблемы с пересечением сигналов (802.11ax)
* **OBSS** - *Overlaped BSS* - перекрывающиеся области радиовидимости.
* **ESS** - *Extended Service Set* - расширенная зона обслуживания. Объединение нескольких точек доступа (несколько сетей BSS).
* **IBSS** - *Independent Basic Service Set (Peer-toPeer)* - станции взаимодействую непосредственно друг с другом.
* **WEP** - 
* **WPA** - 
* **WPA2** -
* **WPA3** - 
* **ISM** -
* **TKIP** - 
* **CCMP (AES)** - 
* **MIC** - *Message Integrity Check* - улучшение безопасности WEP
* **MAC** - *Medium(Media) Access Control* - 2-ой (канальный уровень)
* **MCS** - *Modulation and Coding Scheme* - простое целое число, присваиваемое каждому варианту модуляции.
* **Multipath feding** - многолучевое замирание.
* **4 way handshake** - 
* **WPS** - 
* **OpenSystem** - 
* **OpenAuth** - 
* **SharedKey** - 
* **CAPWAP** - *Control And Proivisoning of Wireless Access Points* - туннельный протокол для взаимодействия контроллера с точкакми доступа (UDP/5246 - control, UDP/5247 - data).


