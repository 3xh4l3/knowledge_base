---
title: "CSMA/SA"
---

* WiFi работает только в half-duplex. Работает в нелицензируемом сильно ограниченном диапазоне, делит эфир с многими другими технологиями - bluetooth, zigbee, микроволновки, радары, т.е. работает в условиях жесткой интерференции.
* Один передает, остальные слушают.

QoS (802.11-2012)
* **DCF** - _Distributed Coordination Function_ - основной, обязательный
    * **HCF** - _Hybrid Coordination Function_ - (optional?)
        * **EDCA** - _Enhanced Distributed Channel Access_ - contention access - устройства конкурируют за доступ к среде.
        * **HCCA** - _Hybrid Coordination Function Controlled Channel Acess_ - controlled access. 
    * **PCF** - _Point Coordination Function_ - точка доступа выступает координатором и сама полит устройства.

Защитный интервал перед отправкой складывается из:
* **DIFS** - _DCF Interframe Space_ - обязательный защитный интервал, который станция выжидает, прежде чем начать передавать.
* **Backoff time** - случайный промежуток обратного отсчета. `
  Backoff time = (Random[CWmin, CWmax]ЅSlotTime.`

* MU-MIMO - _Multi User - Multiple Input Multiple Output_ - 
* Airtime fairness - решение проблемы "медленного клиента". Клиенты ограничиваются (передача данных ими) не по количеству переданных пакетов, а по времени, которое они занимают в эфире.