---
title: IPSec
---

# IPSec L2TP тоннель в Linux

Что требуется:

- прешаред ключ (PSK, shared secret, RSA ключи, сертифика) 
- адрес VPN сервера
- логинпасс для CHAP аутентификации
- openswan - IPsec имплементация для Linux.

## Ubuntu 16.04

```
apt-get install openswan xl2tpd
```

**/etc/ipsec.conf**

```
config setup

conn %default
  kelifetime=60m
  keylife=20m
  rekeymargin=3m
  keyingtries=1
  keyexchange=ikev1
  authby=secret
  ike=aes128-sha1-modp2048!
  esp=aes128-sha1-modp2048!

conn <connection name> 
  keyexchange=ikev1
  left=%defaultroute
  auto=add
  authby=secret
  type=transport
  leftprotoport=17/1701
  rightprotoport=17/1701
  right=<vpn server ip>
```

**/etc/ipsec.secrets**

```
: PSK "<ipsec secret>"
```

```chmod 600 /etc/ipsec.secrets```


**/etc/xl2tpd/xl2tpd.conf**

```
[lac <connection name>]
lns = <vpn server ip>
ppp debug = yes
pppoptfile = /etc/ppp/options.l2tpd.client
length bit = yes
```

**/etc/ppp/options.l2tpd.client**

```
ipcp-accept-local
ipcp-accept-remote
refuse-eap
require-chap
noccp
noauth
mtu 1280
mru 1280
noipdefault
defaultroute
usepeerdns
connect-delay 5000
name <login>
password <password>
```

```chmod 600 /etc/ppp/options.l2tpd.client```

Создаем файл контроля подключением и перезапускаем сервисы

```
mkdir -p /var/run/xl2tpd
touch /var/run/xl2tpd/l2tp-control
service strongswan restart
service xl2tpd restart
```

Подключение

```
ipsec up <connection name>
echo "c <connecton name>" > /var/run/xl2tpd/l2tp-control
```

Отключение

```
echo "d <connection name>" > /var/run/xl2tpd/l2tp-control
ipsec down <connection name>
```

Скрипт автозаупска `~/bin/tuns.sh`

```
#!/bin/bash

# Тоннель до домашнего роутера Mikrotik
IF='home'
SUDO=`which sudo`
IP="$SUDO $(which ip)"
FW="$SUDO $(which iptables)"
IPSEC="$SUDO $(which ipsec)"

# Check interface is exist
IF_EXIST=`ip l | grep -E ".+${IF}(@|:)"`

if [[ $IF_EXIST ]]; then
	echo "Interface ${IF} exist. Delete."
	${IP} link delete ${IF}
fi	

echo "Create interface ${IF}"
${IP} tun add name $IF mode gre \
	local 191.206.171.132 \
	remote 191.206.171.134

${IP} address add 10.7.0.1/30 dev ${IF} 
${IP} link set $IF up

RULE='POSTROUTING -t nat -s 192.168.88.0/24 ! -d 192.168.68.0/23 -j MASQUERADE'

${FW} -C ${RULE} > /dev/null 2>&1
if [[ ! $? -eq 0 ]]; then
	echo 'Nat rule doesnt exists. Create'
	${FW} -I ${RULE}
fi

# IPsec l2tp VPN до Красноярского шлюза (настройки в конфигах)
${IPSEC} down telecom > /dev/null &&
echo "d telecom" | sudo tee /var/run/xl2tpd/l2tp-control
sleep 3
${IPSEC} up orionkrk > /dev/null &&
echo "c telecom" | sudo tee /var/run/xl2tpd/l2tp-control

sleep 3

# Тут добавим нужные маршруты
# Домашняя подсеть
${IP} route add 192.168.88.0/24 via 10.7.0.2
# Служебные
${IP} route add 100.226.249.0/24 dev ppp0
${IP} route add 100.226.254.0/24 dev ppp0
${IP} route add 100.226.255.0/24 dev ppp0
```