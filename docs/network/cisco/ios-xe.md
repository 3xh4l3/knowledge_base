---
title: IOS XE
---

# IOS XE

## Настройка доступа

**aaa new-model** - принуждает использовать на маршрутизаторе локальное имя пользователя и пароль при отсутствии других инструкций AAA

**login local** - то же самое, что и *aaa new-model*

**transport input ssh** - ограничить вход на устройство только по SSH

AAA по локальной базе, доступ к устройству только по `SSH`

```
hostname R1
ip domain-name example.org
crypto key generate rsa modulus 1024
!
service password-encryption
service tcp-keepalives-in
service tcp-keepalives-out
!
aaa new-model
aaa authentication login default local
aaa authorization exec default local
!
enable algorithm-type scrypt secret cisco
username admin algorithm-type scrypt secret cisco
!
line vty 0 4
 transport input ssh
 access-class 50 in vrf-also
 exec-timeout 10 0
 logging synchronous
```
### AAA
Настроиваем Tacacs+.
Tacacs+ лучше чем Radius, потому что шифрует не только пароль, но и имя пользователя при передаче по сети. А так же tacacs+ поддерживает accounting.
Настройка Tacacs+
```
aaa new-model
!
aaa group server tacacs+ TACACS-SRV
 server name SRV1
 server name SRV2
!
aaa authentication suppress null-username
aaa authentication login default group TACACS-SRV local
aaa authentication enable default group TACACS-SRV enable
aaa authorization console
aaa authorization exec default group TACACS-SRV local if-authenticated
aaa authorization commands 15 default group TACACS-SRV local if-authenticated
aaa accounting exec default start-stop group TACACS-SRV
aaa accounting commands 15 default start-stop group TACACS-SRV
!
login on-failure log
login on-success log
```

### Логирование

Логирование введенных пользователем комманд

```bash
archive
 log config
  logging enable
  logging size 250
  notify syslog contenttype plaintext
  hidekeys
```

Общие настройки

```bash
logging userinfo
logging console critical
logging event link-status global
logging event trunk-status global
ip ssh logging events
logging origin-id hostname
logging source-interface Loopback0
logging host 1.1.1.1
logging host 2.2.2.2
logging host 10.226.255.2 vrf mgmtVrf
line vty 0 15
 # Чтобы вывод логов не мешал вводу команд. Возвращает то, что вводил пользователь после вывода лог сообщения.
 logging synchronous level all

```

## route-map

Нюансы работы с `route-map`.

### continue

Нужно помнить, что данная настройки отключает `impicit deny`. Если префикс заматчился правилом, в котором указано `continue`, но не попал под то правило, которые указано следующим (или неявно следующим, то есть без указания `seq`), то префикс пропускается.

Пример:

```bash
# Имеем следующие маршруты в BGP
*>   2.2.2.2/32       0.0.0.0                  0         32768 ?
*>   10.10.0.0/24     0.0.0.0                  0         32768 ?
*>   100.100.100.1/32 0.0.0.0                  0         32768 ?
*>   100.100.100.2/32 0.0.0.0                  0         32768 ?
*>   100.100.100.3/32 0.0.0.0                  0         32768 ?
*>   172.16.0.0       0.0.0.0                  0         32768 ?
*>   172.17.16.0/21   0.0.0.0                  0         32768 ?
*>   192.168.100.0/29 0.0.0.0                  0         32768 ?
*>   192.168.200.0/23 0.0.0.0                  0         32768 ?

# И следующие route-map и prefix-list
route-map RM-R1-OUT permit 10
 match ip address prefix-list PL-GE-24
 set as-path prepend 200
!
route-map RM-R1-OUT permit 20
 match ip address prefix-list PL-R1-OUT
!
route-map RM-R1-OUT deny 30

!
ip prefix-list PL-GE-24 seq 5 permit 0.0.0.0/0 ge 24
!
ip prefix-list PL-R1-OUT seq 5 permit 10.0.0.0/8 le 24
ip prefix-list PL-R1-OUT seq 10 permit 192.168.0.0/16 le 24
ip prefix-list PL-R1-OUT seq 15 permit 100.100.0.0/16 le 24
ip prefix-list PL-R1-OUT seq 20 permit 172.16.0.0/12 le 24
!

# Что анонсируем в итоге
*>   10.10.0.0/24     0.0.0.0                  0         32768 ?
*>   172.16.0.0       0.0.0.0                  0         32768 ?
*>   172.17.16.0/21   0.0.0.0                  0         32768 ?
*>   192.168.200.0/23 0.0.0.0                  0         32768 ?

# Что получаем на другой стороне
*>   10.10.0.0/24     192.168.0.2              0             0 200 200 ?
*>   172.16.0.0       192.168.0.2              0             0 200 ?
*>   172.17.16.0/21   192.168.0.2              0             0 200 ?
*>   192.168.200.0/23 192.168.0.2              0             0 200 ?

```

В данном случае благодаря `deny 30` префиксы длиннее /24 не пропустятся не смотря на то, что они не заматчились в `permit 20`.

Что произошло:

1. Все маршруты длиннее или равные /24 маске препендятся, но на этом проверка не заканчивается, т.к. указано `continue`
2. В `permit 20` к отловленным в `permit 10` префиксам добавляются те, которые попадают под условия префикс листа `PL-R1-OUT`.
3. Те префиксы, которые не попали под `permit 20` запрещаются `deny 30`. Если бы не было `deny 30`, то все отловленные `permit 10`  начали бы анонсироваться соседу (нет implicit deny).

Пример в виде картинки, как это работает:

![route-map](ios-xe.assets/route-map-1619516514345.png)

То есть в данном случае пройдут только /24 и /32 префиксы, при этом на /24 навешается атрибут local-preference 200. Если бы не был явно указан deny 30, то прошли бы и /16 префиксы, т.к. уже были прущены в permit 10.

### Views

Пользователю можно назначить область просмотра/редактирования и свой собственный secret.

```bash
parser view JUNIOR
 secret cisco
 commands exec include configure terminal
 commands exec include show running-config
 commands configure include interface Gi2
 commands configure include interface Gi3
 commands configure ip route
 commands interface include ip address
 commands interface include description
 commands interface include shut
```

Проверяем

```bash
R1#enable view JUNIOR
Password:

R1#?
Exec commands:
  configure  Enter configuration mode
  do-exec    Mode-independent "do-exec" prefix support
  enable     Turn on privileged commands
  exit       Exit from the EXEC
  show       Show running system information

R1#configure terminal
Enter configuration commands, one per line.  End with CNTL/Z.
R1(config)#?
Configure commands:
  do-exec    To run exec commands in config mode
  end        Exit from configure mode
  exit       Exit from configure mode
  interface  Select an interface to configure
  ip         Global IP configuration subcommands

! Not allowed to configure Gi1
R1(config)#interface g1
                      ^
% Invalid input detected at '^' marker.

R1(config)#interface g3
R1(config-if)#?
Interface configuration commands:
  description  Interface specific description
  exit         Exit from interface configuration mode
  ip           Interface Internet Protocol config commands
  no           Negate a command or set its defaults
  shutdown     Shutdown the selected interface

R1(config-if)#description JUNIOR was here
R1(config-if)#ip address 192.168.1.1 255.255.255.0
R1(config-if)exit
R1(config)#ip route 10.10.1.0 255.255.255.0 10.1.255.2
R1(config-if)end

! We are only allowed to see items we have rights to configure
R1#sh running-config
Building configuration...
...
interface GigabitEthernet2
 description TEST
 ip address 10.1.255.1 255.255.255.0
!
interface GigabitEthernet3
 description JUNIOR was here
 ip address 192.168.1.1 255.255.255.0
!
ip route 10.10.1.0 255.255.255.0 10.1.255.2
!
end
```

Видно, что пользователь в `running-config` может видеть только то, что доступно ему для настройки.

## Полезняшки

Заменить сочетание клавиш `Ctr+Shift+6` на привычное `Ctr+c`
```
line vty 0 15
 escape-character 3
```

Настройка устройства для изменения конфига через Napalm (лучше вариант с подменой конфига на IOS XE не использовать - возможен непроизвольный ребут коробки).
```
username salt algorithm-type scrypt secret salt
username salt privilege 15
archive
 path flash:archive
 write-memory
ip scp server enable
```

## Tshoot

Посмотреть утилизация tcam

```
show platform tcam utilization
```

