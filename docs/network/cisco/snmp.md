---
title: SNMP
---

# SNMP

При добавлении `snmp view` Cisco переводит все в буквенный формат. При этом от модели к модели границы возможного буквенного представления могут меняться.

## Сенсоры

Данные хранятся здесь [CISCO-ENTITY-SENSOR-MIB](http://www.circitor.fr/Mibs/Html/C/CISCO-ENTITY-SENSOR-MIB.php)

```bash
ciscoEntitySensorMIB                1.3.6.1.4.1.9.9.91 # ciscoMgmt.91.* на WS-C2960G-24TC-L
entitySensorMIBObjects              1.3.6.1.4.1.9.9.91.1
entSensorValues                     1.3.6.1.4.1.9.9.91.1.1
entSensorThresholds 	            1.3.6.1.4.1.9.9.91.1.2
entSensorGlobalObjects             	1.3.6.1.4.1.9.9.91.1.3
entSensorValueTable 	            1.3.6.1.4.1.9.9.91.1.1.1
entSensorValueEntry                 1.3.6.1.4.1.9.9.91.1.1.1.1
entSensorType 	1.3.6.1.4.1.9.9.91.1.1.1.1.1
entSensorScale 	1.3.6.1.4.1.9.9.91.1.1.1.1.2
entSensorPrecision 	1.3.6.1.4.1.9.9.91.1.1.1.1.3
entitySensorMIBNotificationPrefix   1.3.6.1.4.1.9.9.91.2
entitySensorMIBConformance 	        1.3.6.1.4.1.9.9.91.3

```

Индексы интерфейсов смотреть здесь `SNMPv2-SMI::mib-2.47.1.1.1.1.7`

Нюансы:

* Nexus-ы отдают скейл в милли, то есть значение нужно поделить на 1000. Есть нюанс, т.к. точность указывает на то, что запятую нужно поставить перед третьей цифрой с конца.
* Catalyst-ы отдает без скейла, но значение нужно поделить на 10.

## Chassis

[CISCO-STACK-MIB](https://cric.grenoble.cnrs.fr/Administrateurs/Outils/MIBS/?module=CISCO-STACK-MIB&fournisseur=CISCO)

Cisco C4900

```
snmp-server view check cisco.5.1.2.13.* included
```



## CPU

cpmCPUTotalEntry - Cisco Process Monitor CPU Total Entry
`1.3.6.1.4.1.9.9.109.1.1.1.1`

C3650

```c
snmp-server view default-ro cpmCPUTotalEntry.* included
```



## STP

[CISCO-STP-EXTENSIONS-MIB](https://www.oidview.com/mibs/9/CISCO-STP-EXTENSIONS-MIB.html)

C2960X, C2960S, 

```
snmp-server view default-ro stpxPVSTVlanEnable included
```

C2960S

```
```



## VLANы

 [CISCO-VTP-MIB](http://www.cisco.com/cgi-bin/Support/Mibbrowser/mibinfo.pl?mn=CISCO-VTP-MIB)

