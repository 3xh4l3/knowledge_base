---
title: IOS-XR
---

# IOS-XR

https://xrdocs.io/ - сайт, посвященный IOS XR



## SSH

### Авторизация по ключу

```bash
username user1
  group netadmin
  secret pa$$w0rd
```

Далее перегоняем свой `pub` ключи в бинарный вид `base64`, закидываем на Cisco  и там импортируем

```bash
cat id_rsa.pub | cut -f 2 -d ' ' | base64 -d > xrkey.bin
```

```
# copy ftp://1.1.1./xrkey.bin harddisk:/
(admin)#crypto key import authentication rsa username user1 harddisk:/xrkey.bin
```



