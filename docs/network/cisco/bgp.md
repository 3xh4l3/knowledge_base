---
title: BGP
---

# BGP

## Regex

```bash
| ^   | Start of string
| $   | End of string
| []  | Range of characters
| -   | Used to specify range ( i.e. [0-9] )
| ( ) | Logical grouping
| .   | Any single character
| *   | Zero or more instances
| +   | One or more instance
| ?   | Zero or one instance
| _   | Comma, open or close brace, open or close parentheses, start or end of string, or space
+------------------------------------------------------+
```

Примеры

```bash
.*       - Anything
^$       - Locally originated routes
^100_    - Learned from AS 100
_100$    - Originated in AS 100
_100_    - Any instance of AS 100
^[0-9]+$ - Directly connected ASes

```

## table-map

Настройка, которая позволяет фильтровать или менять параметры тех префиксов, которые помчены для установки в RIB.

```bash
# Отфильтровать все префиксы, имеющие в третьем октете четное число.
ip access-list standard AL_FILTER_ODD
 permit 0.0.1.0 255.255.254.255

route-map RM_TM_FILTER deny 10
 match ip address AL_FILTER_ODD
route-map RM_TM_FILTER permit 20

router bgp 1
 table-map RM_TM_FILTER filter

###
RR#sh ip bgp | b Network
     Network          Next Hop            Metric LocPrf Weight Path
 *>i 110.0.0.0/24     10.1.1.2                 0    100      0 ?
 *>i 110.0.1.0/24     10.1.1.2                 0    100      0 ?
 *>i 110.0.2.0/24     10.1.1.2                 0    100      0 ?
 *>i 110.0.3.0/24     10.1.1.2                 0    100      0 ?
 *>i 120.0.0.0/24     10.1.1.6                 0    100      0 ?
 *>i 120.0.1.0/24     10.1.1.6                 0    100      0 ?
 *>i 120.0.2.0/24     10.1.1.6                 0    100      0 ?
 *>i 120.0.3.0/24     10.1.1.6                 0    100      0 ?

RR#sh ip route | in B.*1[12]0\.
B        110.0.0.0 [200/0] via 10.1.1.2, 00:00:01
B        110.0.2.0 [200/0] via 10.1.1.2, 00:00:01
B        120.0.0.0 [200/0] via 10.1.1.6, 00:00:01
B        120.0.2.0 [200/0] via 10.1.1.6, 00:00:01
```

Такая техника может быть полезна, когда есть выделенный RR, не имеющий достаточно ресурсов установить все префиксы в таблицу маршрутизации.