---
title: N7K
---

Cisco Nexus 7000 Series NX-OS High Availability Commands

https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/5_x/nx-os/high_availability/command/reference/ha_commands.html#wp1373972