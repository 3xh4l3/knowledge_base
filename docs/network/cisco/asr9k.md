---
title: ASR9K
---

## SNMP

Описание работы процессов SNMP на IOS-XR можно посмотреть [здесь](https://community.cisco.com/t5/service-providers-documents/asr9000-xr-understanding-snmp-and-troubleshooting/ta-p/3144621)

Бывает так, что SNMP перестает отвечать на какие-либо запросы. Можно перезагрузить

```c
(admin)# process restart snmpd
(admin)# process restart mibd_route
```
