---
title: ACL
---

## Отличия ip access-group in/out на vlan интерфейсе

```
int
 ip access-group 1 out
!
access-list 1 permit host <ip>
```

Для клиента, подключенного к `int`

- Будут доступны все IP интерфейсы, настроенные на маршрутизаторе (ACL не срабатывает)
- Будет доступен `ip`, настроенный в ACL (ACL срабатывает)
- Весь остальной трафик дропнется

```
int
 ip access-group 100 in
!
access-list 100 permit any host <ip>
```

Для клиента, подключенного к `int`

- Будет доступен только `ip`, настроенный в ACL
- Весь остальной трафик дропнется