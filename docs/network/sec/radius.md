---
title: RADIUS
---

# RADIUS

## EAP

* Extensible Authentication Protocol 
* PEAP - Protected EAP - атрибуты сертификатов передаются зашифрованными. Работает в двух фазах.

![802.1x (Port Based Network Access Control) ⋆ IpCisco](radius.assets/802.1x-EAP-RADIUS-Messaging.jpg)

