---
title: 802.1X
---

Методы аутентификации:

* **Single-host mode**: only a single source MAC address can be authenticated. When the switch detects another source MAC address after authentication, it triggers a security violation. This is the default setting.
* **Multi-domain authentication host mode**: you can authenticate two source MAC addresses, one in the voice VLAN and another one in the data VLAN. This is for the scenario where you have an IP phone and a PC on a single switchport. Any more source MAC addresses trigger a security violation.
* **Multi-authentication host mode**: you can authenticate multiple source MAC addresses. You can use this when your switchport is connected to another switch. Each source MAC address is separately authenticated.
* **Multi-host mode**: the switch allows multiple source MAC addresses. Only the first source MAC address is authenticated, all other source MAC addresses are automatically permitted.