---
title: IPv4
---

# IPv4

## DAI (Dynamic ARP Inspection)

Cisco

```bash
errdisable recovery cause arp-inspection
!
interface GigabitEthernet1/0/2
  ip arp inspection limit rate 8 burst interval 4
```

