---
title: Scapy
---

# Scapy

```python
a = IPv6()
b = ICMPv6ND_NA()

pkt = a/b

# Просто отправить пакет
send(pkt)

# Отправить пакет и обработать ответ
ans, unans = sr(pkt)
# Сделать то же самое в цикле до прерывания процесса
ans, unans = srloop(pkt)
# Результат
ans.summary()

# Отлавливать пакеты на интерфейсе. В лямбде передается фильтр.
pkts=sniff(iface="eth0",lfilter = lambda x: x.haslayer(IPv6))
```

