---
title: IPv6 Toolkit
---

# IPv6 Toolkit

https://www.si6networks.com/research/tools/ipv6toolkit/

> The SI6 Networks’ IPv6 toolkit is a set of IPv6 security assessment  and trouble-shooting tools. It can be leveraged to perform security  assessments of IPv6 networks, assess the resiliency of IPv6 devices by  performing real-world attacks against them, and to trouble-shoot IPv6  networking problems. The tools comprising the toolkit range from  packet-crafting tools to send arbitrary Neighbor Discovery packets to  the most comprehensive IPv6 network scanning tool out there (our scan6  tool).

## Что умеет

### scan6

Поиск адресов на линке.

```bash
# Искать link-local адреса
scan6 -L -i eth0
# Показать MAC адреса для найденных адресов
scan6 -L -i eth0 -e
```

