---
title: Bogons
---

## IPv4

|   Address block    |      Scope      |                         Description                          |
| :----------------: | :-------------: | :----------------------------------------------------------: |
|     0.0.0.0/8      |    Software     | Current network[[1\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc6890-1) (only valid as source address). |
|     10.0.0.0/8     | Private network | Used for local communications within a [private network](https://en.wikipedia.org/wiki/Private_network).[[2\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc1918-2) |
|   100.64.0.0/10    | Private network | [Shared address space](https://en.wikipedia.org/wiki/IPv4_shared_address_space)[[3\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc6598-3) for communications between a service provider and its subscribers when using a [carrier-grade NAT](https://en.wikipedia.org/wiki/Carrier-grade_NAT). |
|    127.0.0.0/8     |      Host       | Used for [loopback addresses](https://en.wikipedia.org/wiki/Loopback_address) to the local host.[[1\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc6890-1) |
|   169.254.0.0/16   |     Subnet      | Used for [link-local addresses](https://en.wikipedia.org/wiki/Link-local_address)[[4\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc3927-4) between two hosts on a single link when no IP address is otherwise specified, such as would have normally been retrieved from a [DHCP](https://en.wikipedia.org/wiki/DHCP) server. |
|   172.16.0.0/12    | Private network | Used for local communications within a private network.[[2\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc1918-2) |
|    192.0.0.0/24    | Private network | IETF Protocol Assignments.[[1\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc6890-1) |
|    192.0.2.0/24    |  Documentation  | Assigned as TEST-NET-1, documentation and examples.[[5\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc5737-5) |
|   192.88.99.0/24   |    Internet     | Reserved.[[6\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc7526-6) Formerly used for [IPv6 to IPv4](https://en.wikipedia.org/wiki/6to4) relay[[7\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc3068-7) (included [IPv6](https://en.wikipedia.org/wiki/IPv6) address block [2002::/16](https://en.wikipedia.org/wiki/IPv6_Address#Special_addresses)). |
|   192.168.0.0/16   | Private network | Used for local communications within a private network.[[2\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc1918-2) |
|   198.18.0.0/15    | Private network | Used for benchmark testing of inter-network communications between two separate subnets.[[8\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc2544-8) |
|  198.51.100.0/24   |  Documentation  | Assigned as TEST-NET-2, documentation and examples.[[5\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc5737-5) |
|   203.0.113.0/24   |  Documentation  | Assigned as TEST-NET-3, documentation and examples.[[5\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc5737-5) |
|    224.0.0.0/4     |    Internet     | In use for [IP multicast](https://en.wikipedia.org/wiki/IP_multicast).[[9\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc5771-9) (Former Class D network). |
|    240.0.0.0/4     |    Internet     | Reserved for future use.[[10\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc3232-10) (Former Class E network). |
| 255.255.255.255/32 |     Subnet      | Reserved for the "limited [broadcast](https://en.wikipedia.org/wiki/Broadcast)" destination address.[[1\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc6890-1)[[11\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc919-11) |

## IPv6

| Address block (CIDR) |  First address   |              Last address               |                     Number of addresses                      |      Usage      |                           Purpose                            |
| :------------------: | :--------------: | :-------------------------------------: | :----------------------------------------------------------: | :-------------: | :----------------------------------------------------------: |
|         ::/0         |        ::        | ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff |                             2128                             |     Routing     |                        Default route.                        |
|        ::/128        |        ::        |                   ::                    |                              1                               |    Software     |                     Unspecified address.                     |
|       ::1/128        |       ::1        |                   ::1                   |                              1                               |      Host       | [Loopback address](https://en.wikipedia.org/wiki/Loopback_address) to the local host. |
|    ::ffff:0:0/96     |  ::ffff:0.0.0.0  |         ::ffff:255.255.255.255          | [2128−96 = 232](https://en.wikipedia.org/wiki/Power_of_2) = 4294967296 |    Software     |                    IPv4 mapped addresses.                    |
|   ::ffff:0:0:0/96    | ::ffff:0:0.0.0.0 |        ::ffff:0:255.255.255.255         |                             232                              |    Software     |                  IPv4 translated addresses.                  |
|     64:ff9b::/96     | 64:ff9b::0.0.0.0 |        64:ff9b::255.255.255.255         |                             232                              | Global Internet | IPv4/IPv6 translation.[[12\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc6052-12) |
|       100::/64       |      100::       |        100::ffff:ffff:ffff:ffff         |                             264                              |     Routing     | Discard prefix.[[13\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc6666-13) |
|      2001::/32       |      2001::      |   2001::ffff:ffff:ffff:ffff:ffff:ffff   |                             296                              | Global Internet | [Teredo tunneling](https://en.wikipedia.org/wiki/Teredo_tunneling). |
|     2001:20::/28     |    2001:20::     |  2001:2f:ffff:ffff:ffff:ffff:ffff:ffff  |                             2100                             |    Software     | [ORCHIDv2](https://en.wikipedia.org/w/index.php?title=ORCHIDv2&action=edit&redlink=1).[[14\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc7343-14) |
|    2001:db8::/32     |    2001:db8::    | 2001:db8:ffff:ffff:ffff:ffff:ffff:ffff  |                             296                              |  Documentation  | Addresses used in documentation and example source code.[[15\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc3849-15) |
|      2002::/16       |      2002::      | 2002:ffff:ffff:ffff:ffff:ffff:ffff:ffff |                             2112                             | Global Internet | The [6to4](https://en.wikipedia.org/wiki/6to4) addressing scheme (now deprecated).[[6\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc7526-6) |
|       fc00::/7       |      fc00::      | fdff:ffff:ffff:ffff:ffff:ffff:ffff:ffff |                             2121                             | Private network | [Unique local address](https://en.wikipedia.org/wiki/Unique_local_address).[[16\]](https://en.wikipedia.org/wiki/Reserved_IP_addresses#cite_note-rfc4193-16) |
|      fe80::/10       |      fe80::      | febf:ffff:ffff:ffff:ffff:ffff:ffff:ffff |                             2118                             |      Link       | [Link-local address](https://en.wikipedia.org/wiki/Link-local_address#IPv6). |
|       ff00::/8       |      ff00::      | ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff |                             2120                             | Global Internet | [Multicast address](https://en.wikipedia.org/wiki/Multicast_address#IPv6). |
|       ff02::1        |                  |                                         |                                                              |                 |                   Все узлы в канале связи                    |
|       ff02::2        |                  |                                         |                                                              |                 |              Все маршрутизаторы в канале связи               |



