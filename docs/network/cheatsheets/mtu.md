---
title: MTU
---



### Non encapsulated

![TCP MSS](MTU.assets/MTU-image-1.png)

### GRE encaplsulated

![TCP MSS](MTU.assets/MTU-image-2.png)

### Ethernet 

![img](MTU.assets/913b516122a6c0252b2a445f42e8f697.jpg)



### Length for various headers, MTU and payload

- **Frame** = HW MTU + FCS + L2(Ethernet) header

- **HW MTU** (Hardware) - default 1500 байт (Ethernet) = (L2 header) + IP header + TCP header + Payload
- **Ethernet MTU** - частный случай HW MTU 
- **IP MTU** = IP header + TCP Header + Payload 
- **MPLS MTU** = MPLS header + HW MTU

- **IP** Header - 20 bytes

- **Ethernet** header -  14 bytes (src mac + dst mac +ethertype) 
- **Preamble** - 8 bytes (не является частью Ethernet заголовка)
- **FCS**(CRC) - 4 bytes
- **TCP** header 20 bytes

- **TCP MSS** (payload) - 1460 bytes (максимальный размер БЕЗ TCP заголовка)

- **GRE** header - 4 bytes
- **MPLS** header - 4 bytes per label
- **IPsec ESP** header - 8 bytes ??
- **ICMP** header - 8 bytes

### Особенности

- Windows/JunOS/Linux/FreeBSD не учитывают при пинге ICMP  и IP заголовки (чистый пейлоад)

  То есть `ping -l 1472`в Win будет генерить пакеты размером 1472+8+20=1500 байт (payload+icmp+ip)

- Cisco при пинге учитывает заголовок ICMP + IP, то есть ping size 1500 это уже payload+icmp+ip

- IOS и IOS XE HW MTU (Ethernet MTU) не включает L2 заголовок

- IOS XR и JunOS HW MTU (Ethernet MTU) включает L2 заголовок

- Если IP MTU совпадает с HW MTU, то в выводе `show run` ip mtu не отображается

### Cases

- For GRE in Cisco we use `ip tcp mss-adjust 1436` because `1460` of standart MSS need to be substracted with `4` bytes of GRE header and `20`bytes of IP header. (Потому что GRE инкапсулирует в себя IP заголовок)

- Посмотреть MTU  в Windows `netsh interface ipv4 show subinterfaces`

