---
title: Routing
---

# Routing

## BGP

### Multipath

В следующюей схеме на P1  и P2 в RIB установится только один маршрут до разделяемого адреса 8.8.8.8 (от CE1 через PE1 соответственно), так как у него меньший `Router-ID`, так как P1 и P2 не имеют прямого ==eBGP== соседства с маршрутизаторами CE1 и CE2 и получают маршруты по ==iBGP==.
![Multipath](routing.assets/Multipath.png)


### Additional Path