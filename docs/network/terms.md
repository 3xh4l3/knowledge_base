---
title: "Термины"
---

**Local Broadcast** - dst:255.255.255.255 работает в пределах бродкаст домена

**Directed Broadcast** - dst:xxx.xxx.xxx.255, то есть направлен на конкретную сеть и рассылается маршрутизатором, на котором настроена это сеть

> Для безопасности необходимо всегда отключать directed broadcast
> В **Linux** опция sysctl - net.ipv4.icmp_echo_ignore_broadcasts = 1

**BIA** - Burned In Address (MAC адрес)