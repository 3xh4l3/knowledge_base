---
title: "Основы"
---

## Режимы работы

## Основные команды

Общие команды для обслуживания сетевого оборудования Huawei.

Режим конфигурирования

```sh
system-view
```

Выйти из контекста

```sh
quit
```

Выйти в user-view

```sh
return
```

История команд

```sh
display history-command
reset history-command all-users
reset history-command
```

Конфиг

```sh
# Показать весь конфиг или указанного контекста
display current-configuration [options]
# Показать конфиг в текущем контексте
display this
# Сохранить 
save
# Показать параметры загрузки
display startup
# Сбросить конфиг
reset saved-configuration
reboot
```

Диагностическая информация. Большой вывод! Объединяет в себе несколько команд вывода

```sh
display diagnostic-information
->
display clock
display version
...
```

## system-view команды

Задать имя устройства

```sh
sysname <HOSTNAME>
```

Забиндить команды на сочетание клавиш

```sh
hotkey { CTRL_G | CTRL_L | CTRL_O | CTRL_U } command-text
# Показать хоткеи
display hotkey
```

Ограничить доступ к vty

```sh
# Максимальное число подключени
user-interface maximum-vty 21
# Настройка ACL
acl 2000
[acl4-basic-2000] rule deny source 10.1.1.1 0
[acl4-basic-2000] quit
# Применение настроек
user-interface vty 0 21
[ui-vty0-17] acl 2000 inbound
[ui-vty0-17] shell
[ui-vty0-17] idle-timeout 30
[ui-vty0-17] screen-length 30
[ui-vty0-17] history-command max-size 20
[ui-vty0-17] user privilege level 15
[ui-vty0-17] authentication-mode password
[ui-vty0-17] set authentication-mode password
```

Настройка интерфейса

21 vty

license

interface

​	traffic-filter - ACL

​	traffic-policy - политики

undo = no

**ospf**

silent-interface (в первую очередь)

```
#
ospf 1 router-id 172.16.0.1 
 silent-interface all
 undo silent-interface GigabitEthernet0/0/0
 undo silent-interface GigabitEthernet0/0/1
 undo silent-interface LoopBack0
 area 0.0.0.0 
  network 172.16.0.1 0.0.0.0 description Loopback0
#
```

## VRF

```
ip vpn-instance <name>
```

## BGP

```c
# update-source
peer x.x.x.x connect-interface loopback 1
```

## IS-IS

```c
bfd
 quit

vlan 666
 quit

isis 1
 description TEST_ISIS
 is-level level-2
 timer lsp-generation 5 1 50 level-1      
 timer lsp-generation 5 1 50 level-2
 flash-flood 15 level-1
 flash-flood 15 level-2
 bfd all-interfaces enable
 bfd all-interfaces min-tx-interval 100 min-rx-interval 100 detect-multiplier 3
 network-entity 49.0001.0000.0000.0002.00
 timer spf 5 1 50
 quit

interface XGigabitEthernet0/0/1
 port link-type trunk
 port trunk allow-pass vlan 1 666
 quit

interface Vlanif666
 description ====== 4500X ======
 mtu 9198
 ip address 10.66.77.2 255.255.255.252
 isis enable 1
 isis circuit-type p2p
 isis tag-value 100000
 isis circuit-level level-2
 isis authentication-mode md5 plain test
 isis bfd enable
 quit
```

