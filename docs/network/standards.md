---
title: Стандарты
---

Список полезных стандартов и документов, которые полезно всегда иметь под рукой:

* [Реестр протоколов IANA](https://www.iana.org/protocols)
    * [Border Gateway Protocol (BGP) Parameters](https://www.iana.org/assignments/bgp-parameters/bgp-parameters.xhtml)
    * [Internet Protocol Version 4 (IPv4) Parameters](https://www.iana.org/assignments/ip-parameters/ip-parameters.xhtml)

## Общие стандарты

* [RFC4727](https://datatracker.ietf.org/doc/html/rfc4727) (Nov 2006) -  Experimental Values in IPv4, IPv6, ICMPv4, ICMPv6, UDP, and TCP Headers.
