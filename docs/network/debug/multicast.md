---
title: Multicast
---

## Активный источник для группы

Маршрут для `Reverse Path Forwarding` (*RPF*) может содержать как в юникастовой так и мультикастовой таблице маршрутизации, поэтому в первую очередь смотрим для групп, которые мы получаем по `PIM` (или другой протокол маршрутизации групп)

```c
ABK-DC0#show ip mroute 239.254.3.39
...
(*, 239.254.3.39), 7w0d/stopped, RP 10.253.253.253, flags: SJC
  Incoming interface: Null, RPF nbr 0.0.0.0
  Outgoing interface list:
    Vlan808, Forward/Sparse-Dense, 32w5d/00:02:43

(212.49.127.162, 239.254.3.39), 6w1d/00:02:13, flags: MT
  Incoming interface: Vlan78, RPF nbr 8.65.1.38, Mbgp
  Outgoing interface list:
    Vlan808, Forward/Sparse-Dense, 6w1d/00:02:43

```

Флажок **S** говорит нам 

