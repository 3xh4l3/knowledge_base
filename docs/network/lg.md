---
title: "Looking Glass"
---

## Настройка оборудования

### Cisco IOS (XE)

При помощи View

```
router#enable view
Password:

router#config terminal
Enter configuration commands, one per line.  End with CNTL/Z.
router(config)#parser view looking-glass
router(config-view)# secret VIEW-ENABLE-PASSWORD
router(config-view)# commands exec include all traceroute
router(config-view)# commands exec include all ping
router(config-view)# commands exec include all show bgp
router(config-view)# commands exec include show
router(config-view)# exit
router(config)#access-list 1 permit IP4-ADDR-OF-YOUR-LOOKING-GLASS
router(config)#username lg view looking-glass access-class 1 secret LG-USER-PASSWORD
router(config)# end
router# write
```

Авторизация по ключу

```
router(config)#ip ssh pubkey-chain
router(conf-ssh-pubkey)#username lg
router(conf-ssh-pubkey-user)#key-string
router(conf-ssh-pubkey-data)#           ! Input the pubkey BUT WRAP TO ~ 80 CHARS BEFORE PASTING
router(conf-ssh-pubkey-data)# end
```

Нерекомендуемый метод, но единственно возможный на старых устройствах через  систему привелегий

```
router#config terminal
Enter configuration commands, one per line.  End with CNTL/Z.
router(config)# privilege exec all level 4 show bgp
router(config)# privilege exec all level 4 ping
router(config)# privilege exec all level 4 traceroute
router(config)#access-list 1 permit IP4-ADDR-OF-YOUR-LOOKING-GLASS
router(config)#username lg privilege 4 access-class 1 secret LG-USER-PASSWORD
router(config)# end
router# write
```


### Cisco IOS XR

```
taskgroup looking-glass
taskgroup looking-glass task read bgp
taskgroup looking-glass task read basic-services
taskgroup looking-glass task write basic-services
taskgroup looking-glass task execute basic-services
taskgroup looking-glass description "Looking Glass required tasks"
usergroup looking-glass
usergroup looking-glass taskgroup looking-glass
usergroup looking-glass description "Looking Glass users"
username <username>
username <username> group read-only-tg
username <username> group looking-glass
username <username> secret <password>
```

### Cisco NX-OS

```

```

### Juniper JunOS

```
[edit system login]
+    class looking-glass {
+        permissions view-configuration;
+        allow-commands "(show)|(ping)|(traceroute)";
+        deny-commands "(clear)|(file)|(file show)|(help)|(load)|(monitor)|(op)|(request)|(save)|(set)|(start)|(test)";
+        allow-configuration show;
+        deny-configuration all;
+    }
[edit system login]
+    user lg {
+        class looking-glass;
+        authentication {
+            ...
+        }
+    }
```

### Huawei VRP8

```

```
