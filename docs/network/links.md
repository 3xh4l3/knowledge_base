---
title: Ссылки
---

- http://njrusmc.net/ - сайт **Ника Руссо** CCDE, CCIE, признанного эксперта в дизайне IP/MPLS. В данный момент активно развивает автоматизацию и идеолгию Network DevOps.  
http://njrusmc.net/jobaid/jobaid.html - ==инструменты для работы==: дампы трафика, шпаргалки и список ресурсов для сертификации Cisco DevNet

- https://natenka.github.io/ - сайт **Наташи Самойленко**  
https://natenka.github.io/bgp/ - джентльменский набор инструментов для работы с ==BGP==
