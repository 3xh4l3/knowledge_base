---
title: EVPN
---

# EVPN (Ethernet VPN)

* https://e-vpn.io/ - cайт, полностью посвещенный EVPN от Cisco
* https://bgphelp.com/ - достаточно много ссылок и информации по EVPN

## Концепты

- Служит для межсетевого взаимодействия на L2 и L3 уровне.
- Похож на *VPLS*, но не использует *data-plane MAC learning*.
- Изучение *MAC* адресов происходит на *control-plane*. Используется *BGP* сигнализация (*AFI 25 - L2VPN,  SAFI 70 - EVPN*).
- Обменивается *MAC* адресами как *L3VPN* маршрутами (есть *RD*, *RT*).
- *Data-plane*: *MPLS*, VXLAN, PBB, NVO.
- Для DCI возможна интеграция с *VxLAN* (*EVPN over VxLAN*) - в этом случае *VTEP* обнаруживаются средствами *EVPN* (нет необходимости использовать мультикаст)
- Поддерживает `multi homing` (sinle-active, all-active load balancing)
- Возможно использовать для интеграции коммутации и маршрутизации -> множественные точки выхода наружу из *EVPN* домена
- *EVPN* определяет новый *BGP NLRI* - AFI 25 L2VPN, SAFI 70 EVPN

## Уровень Control Plane

- PE при изучении MAC адреса шлет всем другим PE BGP update - MAC + MPLS Lables (VPN Label)
- PE удалении MAC адреса шлет BPG update withdraw
- Если PE получает ARP запрос и находит MAC в локальной таблице, то он отвечает на ARP не рассылая при этом broadcast на остальные PE.

## Возможные режимы работы EVPN

1. По способу мультиплексирования VLAN (RFC7432):
   1. **VLAN-Based Service** - соответствие 1:1 VLAN:EVI (Label per VLAN). Позволяет использовать нормализацию VLAN ID - трансляция VLAN, то есть с двух сторон можно использовать разные VLAN. Оптимальное распространения BUM трафика, большее количество MPLS меток.
   2. **VLAN Bundle Service** - соответствие N:1 VLAN:EVI. EVI - один бродкаст домен. Нормализацию использовать нельзя. MPLS метки выделяются per EVI, то есть в меньшем количестве. Неоптимальное распространение BUM.
   3. **VLAN-Aware Service** - соответствие N:1 VLAN:EVI. Возможна нормализация VLAN. EVI делится на Bridge домены и каждый VLAN асоциируется со своим BD. Большее количество MPLS меток. Оптимальное распространение BUM.
2. По типу инкапсуляции:
   1. **MPLS** -  ==RFC7432 (updated RFC8584)== выделение меток: per mac, per instance, per next-hop
   2. **VxLAN**
   3. **PBB** - Ethernet <- MPLS <- Ethernet <- PBB
   4. **NVO (Network Virtualization Overlay)**  - ==RFC8365==
3. По логической топологии между точками подключения клиентов:
   1. **E-LAN**  - Каждый с каждым - альтернатива VPLS.
   2. **E-TREE** - дерево, есть центральная точка, которая может общаться с переферийными точками.
   3. **E-LINE** - P2P, L2Circuit, VPWS - используются исключительно маршруты первого типа.

## Типы маршрутов EVPN

* *1* - Ethernet Auto-Discovery Route
* *2* - MAC Advertisement Route
* *3* - Inclusive Multicast Route
* *4* - Ethernet Segment Route
* *5* - IP Prefix Advertisement Route

Маршруты служат для задач *control-plane*, а именно:

1. Доступность *MAC/IP*
2. Массовое удаление *MAC* (mass withdrawal) - удаление всех MAC, связанных с ES
3. *Split-Horizon label adv.* - механизм защиты от петель при MH режиме, при получение такой MPLS метки, PE не отправляет трафик обротано в указанный ES
4. *Aliasing* - механизм балансировки трафика при MH режиме, MPLS метка
5. *Multicast endpoint discovery* ❓
6. *Redundancy group discovery* - выбор DF, защита от петель.
7. *Designated forwarder election* - маршруты Type4

Сводная таблица задач, решаемых маршрутами

![image-20200628234857841](evpn.assets/image-20200628234857841.png)

### Type1 (Ethernet Auto-Discovery)

* Появляются только при Multi Home подключениях (ESI != 0).

* Служат для быстрой сходимости, позволя PE быстро изменить next-hop для MAC адресов, асоциированных с ES. Так называемый **MAC Mass Withdraw**. Генерируется **per-ESI**.

* Нужны для балансировки трафика (Aliasing метка) по точкам выхода в сторону CE. Такой маршрут генерируется **per-EVI**. Aliasing метка не используется, пока не получен per-ESI маршрут, в котором указан режим работы (AA/SA). 

* Передает SHL (Split-Horizon Label) в расширенном комьюнити, которую навешивает nonDF маршрутизатор отправляя BUM трафик в сторону DF. DF в сторону nonDF эту метку не навешивает, так как nonDF и так не отправляет BUM трафик в сторону ES. Такой машрут генерируется **per-ESI**

  ![image-20200721222338709](evpn.assets/image-20200721222338709.png)

### Type 2 (MAC/IP Advertisement route)

* Всегда генерируется per EVI и имеет нативный RT и RD инстанса

* Сообщают MAC адреса и возможно IP, которые с ними асоциированы (ARP). То есть указывает машрутизатору, куда и с какой меткой слать юникастовый трафик.

* Аналогичен vpnv4 машруту в L3VPN.

* Для анонса PE должен изучить MAC подключенного CE через обычный data-plane механизм

* MAC адрес так же может быть изучен при взаимодействии через control-plane между PE и CE (3 года назад не было ни одной реализации)

  ![image-20200721222222710](evpn.assets/image-20200721222222710.png)

### Type 3

* Всегда генерируется per EVI и имеет нативный RT и RD инстанса

* Начинают рассылать в первую очередь при запуске EVPN
* Указывает маршрутизатору, куда и с какой меткой слать BUM трафик.

### Type 4

* Всегда генерируется per ESI. RD генерируется из router-id или адреса лупбека, RD из ESI
* Отвечает за выбор DF 
* Используеться только в Multi Home
* Может генерироваться per ESI
* Может генерироваться per EVI

## Варианты подключения

### Multi Home

* Идентичный *ESI* на PE/Leafs.
* Выбор DF для форвардинга трафика осуществляется per *EVI/ESI*.
* DF (Designated Forwarder) отвечает за пересылку BUM трафика в сторону CE - защита от петель.
* DF выбирается по формуле V mod N = i (V номер влана, N количество маршрутизаторов), чем достигается некая балансировка BUM  трафика



![image-20200629000239652](evpn.assets/image-20200629000239652.png)

### Single Home

* Ethernet Segment Identifier (ESI) ‘0’ 
* DF не выбирается

![image-20200629000248479](evpn.assets/image-20200629000248479.png)

## Виды балансировки трафика

### All-Active

Единый LAG на стороне CE VLAN активен на обоих PE Трафик балансируется per flow 

**Преимущества**: Bandwidth, Convergence

![image-20200628235751862](evpn.assets/image-20200628235751862.png)

### Single-Active

Несколько LAGs на стороне CE VLAN активен на одном PE Трафик балансируется per VLAN 

**Преимущества**: Billing, Policing

![image-20200628235833740](evpn.assets/image-20200628235833740.png)

### Port-Active

Единый LAG на стороне CE Порт активен на одном PE Трафик балансируется per port 

**Преимущества**: Protocol Simplification

![image-20200628235854424](evpn.assets/image-20200628235854424.png)

## Терминология

* **BD Bridge Domain** - бродкаст домен
* **EVPN (Ethernet VPN)** - RFC7432
* **EVPN VPWS** - RFC8214 - L2 P2P - соединение точка-точка (Multi/Single-Home, All/Single-Active)
* **EVPN-FXC** - draft-sajassi-bess-evpn-vpws-fxc
* **EVPN-IRB** - draft-ietf-bess-evpn-inter-subnet-forwarding, draft-ietf-bess-evpn-prefix-advertisement
* **EVPN-Overlay** - RFC8365
* **EVPN-Etree** - RFC8317, топология EVPN
* **EVPN-E-LAN** - топология EVPN
* **EVPN E-Line** - топология EVPN, то же, что и EVPN VPWS
* **Ethernet Tag** - идентификатор BD (bd-tag). VLAN или VNI в случае с VxLAN
* **PBB-EVPN** - RFC7623
* **ES (Ethernet Segment)** - набор линков от CE к PE.
* **ESI (Ethernet Segmet Identificator)** - глобальный 10-байтный идентификатор *Ethernet* подключения абонента
  * ESI=0 - single-homed подключение
  * ESI!=0 - multi-homed подключение
  * ESI = (0xFF){10} - зарезервировано
* **EVI (EVPN Instance), MAC-CRF** - идентифицирует VPN, присутствует на всех PE, участвующих в EVPN. В Junos - **virtual-switch**.
* **MAC-VRF** - VRF таблица MAC адресов на PE
* **Port-based EVPN**
* **VLAN-based EVPN**
* **VLAN-bundling EVPN**
* **VLAN aware bundling EVPN**
* **DF (Designated Forwarder)** - RFC7432, RFC8365 - в Multihome схемах маршрутизатор, отвечающий за передачу трафика. Выбирается по *ESI.*
* **SH (Single Home)** - *DF* не выбирается
* **AA MH (All-active Multi-Home)** -  выбирается *DF*  per *EVI/ESI*, при этом на *PE* используется идентичиный *ESI*.
* **Anycast IRB** - 

# Расширения технологии EVPN

## PBB

* **PBB (Provider Backbone Bridging)** - *MAC in MAC* - https://www.juniper.net/documentation/en_US/junos/topics/concept/pbb-evpn-integration-for-dci-overview.html - инкапусляция MAC абонента в 24-битный *I-SID*
  * **I-SID (Instance Service Identifier)**  - 
  * **C-MAC (Customer MAC)** - MAC клиента
  * **S-MAC (Service MAC)** - MAC провайдера
  * **B-MAC (Backbone MAC)** - MAC добавляющийся на границе *PBB* сети
  * **PB (Provider Bridge)** (802.1ad)
  * **PEB (Provider edge bridge)** (802.1ad)
  * **BEB (Backbone edge bridge)** (802.1ah)
  * **BCB (Backbone core bridge)** (802.1ah)

![g043605](evpn.assets/g043605.png)

# Vendor specific

Примеры настройки и особенности работы у разных вендоров.

## Huawei

* **evpn instance b-evpn** - (PBB) - SPE (Superstratum Provider Edge) для связи backbone EVPN и HVPLS (Hierarchial VPLS). Настраивается между PE.
* **evpn instance bd-evpn** - Bridge Domain EVPN
* **evpn instance i-evpn** - (PBB) настраивается на границе с CE, для инкапсуляции трафика клиента в PBB. Назначается I-SID.
* **evpn instance vpws** - режим провода *P2P* (E-LINE)
* **E-Trunk** - нужен для возможности подключить *CE* к двум *PE* (Dualhomed). Соединеяет два *PE* маршрутизатора.
* **VLL (Virtual Lease Line)** - 

## Juniper

- **VLAN Based Service (instance EVPN)** - на каждый VLAN свой routin instance
- **VLAN Aware Service (virtual-switch)** - один инстанс для всех bridge доменов
- **EPL (Ethernet Private Line)** - обеспечивает P2P EVC между парами UNIs с высокой степенью прозрачности.
- **EVPL (Ethernet Virtual Private Line)** - обеспечивает P2P EVC между парами {ESI, VLAN}, поддерживет мультплексирование сервисов - несколько EVC.
- **EVC (Ethernet Virtual Connection)** - виртуально Ethernet соединение между двумя пользовательскими сетями (UNIs - User Network Interfaces)

## Cisco