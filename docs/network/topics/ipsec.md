---
title: IPSec
---

# IPSec

**IPSec** - набор протоколов для аутентификации и шифрования при передаче IP пакетов, разрабатываемый [рабочей группой IETF](https://datatracker.ietf.org/wg/ipsec/about/).

## Стандарты

Список стандартов, разработанных рабочей группой IPSec - https://datatracker.ietf.org/wg/ipsec/documents/

Актуальные стандарты:

- [RFC4301](https://datatracker.ietf.org/doc/html/rfc4301) -  Security Architecture for the Internet Protocol
- [RFC4302](https://datatracker.ietf.org/doc/html/rfc4302) (Dec 2005) - IP Authentication Header
- [RFC4303](https://datatracker.ietf.org/doc/html/rfc4303) (Dec 2005) - IP Encapsulating Security Payload (ESP).
- [RFC8221](https://datatracker.ietf.org/doc/html/rfc8221) -  Cryptographic Algorithm Implementation Requirements and Usage Guidance



## Термины и принцип работы

IP security protocol делится на две части

* **AH** - Authentication Header - защита от изменения пакета, подмены адреса источника. Не шифрует данные. Используется, например, в OSPFv3.

  <img src="ipsec.assets/Authentication-Header-Format.ppm" alt="Authentication Header Format | Download Scientific Diagram" style="zoom:50%;" />

  * **ICV** - *Integrity Check Value* - значение хеш функции для AH, которое служит для проверки целостности пакета на принимающей стороне.

* **ESP** - UDP 500/4500 Encapsulation Security Payload - шифрование данных. Может работать в транспортном и тоннельном режимах.

  <img src="ipsec.assets/slide_6.jpg" alt="Encapsulation Security Payload Protocol Lan Vu. OUTLINE 1.Introduction and  terms 2.ESP Overview 3.ESP Packet Format 4.ESP Fields 5.ESP Modes 6.ESP  packet. - ppt download" style="zoom: 67%;" />
  
  На картинке Authentication Date = ICV.
  
  Стоит отметить, что ESP может обеспечить не только конфиденциальность, но и целостность отдельно без шифрования, что принесет ту же пользу что и AH. Также возможна и обратная ситуация - обеспечение конфиденциальности без проверки целостности, что не рекомендуется.

### Режимы работы

* Транспортный - остается оригинальный IP заголовок. Используется в основном, когда есть туннельные интерфейсы. Шифруется все, что внутри IP пакета.
* Туннельный - IP заголовок и все остальное заворачивается в зашифрованный пейлоад.

### Другие термины

* SA - Security Association - чем шифруем (AES, 3DES) и как аутентифицируем (SHA, MD5), и что шифруем.
* SP - Security Policies - аля правила фаерволла, которые определяют, что делать с прищедшим трафиком, какие пакеты нужно шифровать, дешифровать.
* PF_KEYv2 - API для настройки SA/SP.
* setkey - утилита для ручной настройки SA/SP.
* SPD - Security Policies Database
* SPI - Security Parameter Index - идентификатор ESP тоннеля, по которому определяется нужная SA, как правило, по {SrcIP, DstIP, SPI}.
* ISAKMP - Internet Security Association and Key Management Protocol - фреймворк для согласования SA. Можно сказать, что это control-plane.
* Oakley - 
* IKE - Internet Key Exhange - протокол обмена ключами по-умолчанию для ISAKMP (реализация ISAKMP). Работает в двух фазах. 
  * Main mode - используется для Site to Site
    * Phase 1 - Согласование политик ISAKMP, формирование proposal-ов, обмен ключами, аутентификация в уже зашфрованном канале.
    * Phase 2 - согласование настроек ESP.
  * Aggressive mode - используется для Remote Access с использованием preshred ключей.
    * Phase 1 - проверка identity не происходит
    * Phase 2 - 
* IKEv2 - вторая версия IKE
* ESN - Extended Sequence Number - 64-битный SeqNum, 32 бита которого передаются в ESP заголовке, а остальные 32 бита держаться в памяти. Ендпоинты должны согласовать этот параметр.

Особенности:

* IPSec не создает интерфейсов, поэтому часто используется с другими тоннельными протоколами (не всегда так - vti(6) интерфейсы в Linux, ipsec интерфейсы во FreeBSD).
* ESP работает поверх IP(6) и не работает за NAT, т.к.NAT ничего не знает о сетевом протоколе ESP. Для это придумали оборачивать ESP в UDP/4500 - NAT-T (NAT Traversal).