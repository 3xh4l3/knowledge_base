---
title: OSPF
---

# OSPF

## Стандарты

* **[RFC2328](https://www.rfc-editor.org/rfc/rfc2328.html)** - OSPF Version 2

## Термины

* **LSU** - *Link State Update* - общеназвание сообщений, в которых передаются ==LSA==.
* **SPT** - Shortest Path Tree
* **[PRC](prc.md)** - Partial Route Calculation (в Cisco Partial SPF)
* **[ISPF](ispf.md)** - Incremental SPF