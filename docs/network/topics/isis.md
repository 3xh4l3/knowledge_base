---
title: IS-IS
---

# IS-IS

- IS-IS - Intermediate System to Intermediate System
- IS - это маршрутизатор в терминологии ISO
- Изначально разработан в качестве динамического протокола маршрутизации для [ISO CLNP](https://www.iso.org/standard/30932.html). Позже был адаптирован для передачи IP префиксов ([RFC 1195](https://tools.ietf.org/html/rfc1195))

## Приемущества

* L2 - нет возможности атаковать IGP, используя IP (как OSPF)
* Поддержка IPv6
* Больше возможностей для оптимизации