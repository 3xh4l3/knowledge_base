---
title: QinQ
---

# QinQ

## 802.1ad

Названия:

* Двойная метка
* QinQ
* Stacked VLAN

Термины:

**SP-VLAN (Service Provider)** - внешняя метка (OuterTag)

**CE-VLAN (Customer Edge)** - внутренняя метка (InnerTag)

Внутрення метка не меняет свои вид.

Внешняя может иметь разный EtherType: 

* 0x8100

* 0x88a8 (стандартный)

* 0x9100

* 0x9200

  

