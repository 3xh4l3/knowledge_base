---
title: IPv6
---

# IPv6

## Стандарты

Список всех RFC, касающихся IPv6 - https://www.system.de/ipv6-rfc/.

### Основные

* [RFC8200](https://tools.ietf.org/html/rfc8200) / STD86 (July 2017) - Internet Protocol, Version 6 (IPv6) Specification
* [RFC4443](https://www.rfc-editor.org/rfc/rfc4443.html) (Mar 2006) - Internet Control Message Protocol (ICMPv6) for the Internet Protocol Version 6 (IPv6) Specification
* [RFC4884](https://datatracker.ietf.org/doc/html/rfc4884) (Apr 2007) - Extended ICMP to Support Multi-Part Messages
* [RFC4861](https://www.rfc-editor.org/info/rfc4861) (Sep 2007) - Neighbor Discovery for IP version 6 (IPv6)
* [RFC7045](https://datatracker.ietf.org/doc/html/rfc7045) (Dec 2013) - Transmission and Processing of IPv6 Extension Headers
* [RFC6564](https://datatracker.ietf.org/doc/html/rfc6564) (Apr 2012) - A Uniform Format for IPv6 Extension Headers
* [RFC7112](https://www.rfc-editor.org/rfc/rfc7112.html) (Jan 2014) - Implications of Oversized IPv6 Header Chains
* [Типы расширенных заголовков](http://www.iana.org/assignments/ipv6-parameters/ipv6-parameters.xhtml#extension-header)
* [RFC7872](https://datatracker.ietf.org/doc/html/rfc7872) (Jun 2016) - Observations on the Dropping of Packets with IPv6 Extension Headers in the Real World
* [RFC8504](https://datatracker.ietf.org/doc/html/rfc8504) (Jan 2019) - IPv6 Node Requirements
* [RFC4291](https://www.rfc-editor.org/rfc/rfc4291) (Feb 2006) - IP Version 6 Addressing Architecture
* [RFC7136](https://datatracker.ietf.org/doc/html/rfc7136) (Feb 2014) - Significance of IPv6 Interface Identifiers
* [RFC4191](https://datatracker.ietf.org/doc/html/rfc4191) (Nov 2005) - Default Router Preferences and More-Specific Routes

### Переходные механизмы

* [RFC5569 (Jan 2010)](https://www.rfc-editor.org/info/rfc5569) - IPv6 Rapid Deployment on IPv4 Infrastructures (6rd)
* [RFC5969 (Aug 2010)](https://www.rfc-editor.org/info/rfc5969) - IPv6 Rapid Deployment on IPv4 Infrastructures (6rd) - Protocol Specification

### Лучшие практики

* [RIPE-772](https://www.ripe.net/publications/docs/ripe-772) - требовния к телекоммуникационному оборудованию по поддержке IPv6.
* [draft-ietf-opsec-ipv6-eh-filtering](https://datatracker.ietf.org/doc/html/draft-ietf-opsec-ipv6-eh-filtering) - Recommendations on the Filtering of IPv6 Packets Containing IPv6 Extension Headers at Transit Routers draft-ietf-opsec-ipv6-eh-filtering-08.

## Термины

* **EUI-64** - *Extended Unique Indentifier* - механизм, который позволяет клиенту самостоятельно генерировать 64 битный идентификатор интрфейса.
* **SLAAC** - *Stateless Address Autoconfiguration* - способ конфигурирования хоста без помощи ==DHCPv6==.
* **NB** - *Neighbor Discovery Protocol* - IPv4/ARP/Broadcast -> IPv6/ICMPv6/Multicast
* **GUA** - *Global Unicast Address* - 
* **ULA** - *Uniq Local Address* - 
* **EH** - *Extension Headers* - 
* **NA** - *Neighbor Advertisement* - 
* **NS** - *Neighbor Solicitation* - 
* **RA** - *Router Advertisement* - 
* **RS** - *Router Solicitation* - 
* **DAD** - *Duplicate Address Detection* - 
* **NUD** - *Network Unreachabillity Detecton* - проактивное обновление кеша NDP.
* **IID** - *Interface ID* - 
* **CGA** - *Cryptographically Generated Addresses* - метод генерации IID в SLAAC, используется в *SEND* (Secure Neighbor Discovery).
* **HBA** - *Hash Based Address* - использует симметричное шифрование для генерации IID в SLAAC, используется в SHIM6.

## Хакактеристики

### Заголовок

Заголовок состоит из фиксированного заголовка IPv6, расширения и пользовательских данных. ==Payload== включает в себя как пользовательские данные, так и `Extension headers`.

![image-20210531120252487](ipv6.assets/image-20210531120252487.png)

* **Version** - 4bit - всегда 6 (0110).
* **Traffic Class** - 8bit - то же, что и ==ToS== в IPv4.
* **Flow Label** - 20bit - подсказка для маршрутиазторов и коммутаторов, что пакеты должны ходить по одному пути. Так же рассматривается для борьбы со спуфингом.
* **Next Header** - 8bit - указывает на то, какой заголовок на транспортном уровне, либо на то, что следующим идет ==Extension header==.
* **Hop Limit** - 8bit - то же, что и TTL в IPv4.

![image-20210531121124445](ipv6.assets/image-20210531121124445.png)

#### Сравнение с IPv4

* Улучшена эффективность за счет переноса сложности обработки пакетов на конечные узлы - границы Интернета.
  * Фрагментация пакета осуществляется только источником.
* Некоторые поля больше не нужны, например `header length`.
* Некоторые поля перемещены в _Extension headres_, например `fragmentation`.
* В некоторых полях больше нет необходимости, например `checksum`, т.к. оно расчитывается в заголовке 2-го уровня.

![image-20220104135111700](ipv6.assets/image-20220104135111700.png)

### Extension headers

* Расширенные заголовки опциональны и обычно отсутствуют.
* Может быть и более расширенных заголовков, которые в свою очередь могут иметь различные опции внутри себя.
* Расширенные заголовки объединяются в цепочку, используя поле `Next Header`. Порядок заголовков жестко определен.
* Каждый тип расширенных заголовков может использоваться только 1 раз кроме заголовка `Destination Options`, который может быть указан дважды - перед заголовком EH и/или в конце.
* Расширенные заголовки фиксированы и стандартизованы, хотя в будущем могут появиться и новые. Вместо этого различные опции добавляются в существующие заголовки.
* Все расширенные заголовки обрабатываются на конечных узлах за исключением `Hop-by-hop` и `Routing`.
* Заголовок следующего уровня (`upper-layer`) терминирует цепочку EH, либо это делается выставлением поля `Next Header`=59.

Проблемы:

* Расширенные заголовки усложняют фильтрацию, делая порой невозможным корректно проанализировать всю цепочку EH.

| Заголовок                                    | Описание                                                     |
| -------------------------------------------- | ------------------------------------------------------------ |
| Hop-by-Hop options                           | Обрабатывается всеми устройствами по пути.                   |
| Destination options (before routing header)  | Обрабатывается каждым маршрутизатором, перечисленным в заголовке routing. То есть это опции для заголовка Rouing. |
| Routing                                      | Методы маршрутизации пакета - список IP, через которые должен пройти пакет. Обрабатывается всеми маршрутизаторами, которые есть в списке. Существует [несколько типов маршрутизации](https://www.iana.org/assignments/ipv6-parameters/ipv6-parameters.xhtml#ipv6-parameters-3), в числе которых есть и SR. |
| Fragment                                     | Параметры фрагментации пакета.                               |
| Authentication                               | Содержит информацию для аутентификации.                      |
| Encapsulating Security Payload (ESP)         | Зашифрованные данные.                                        |
| Destination options (before upper-layer PDU) | Обрабатывается только получателем пакета.                    |

![image-20210531135549025](ipv6.assets/image-20210531135549025.png)

![image-20220104154311925](ipv6.assets/image-20220104154311925.png)

### **Структура адреса**

* **Network Prefix** - Адрес сети - левые 64 бита
    * Длина префикса
    * Количество бит для дальнешейго разделения на подсети
* **Interface Indentifier** - Идентификатор интерфейса - правые 64 бита

### **Типы адресов**

* Unicast
* Multicast - начинается с **FF** (8бит). Следующие 4 бита - флаги, далее 4 бита - область распространения.
* Anycast
* Широквещательные адреса отсутствуют

Список зарезервированных адресов http://www.iana.org/assignments/iana-ipv6-special-registry/iana-ipv6-special-registry.xhtml

Список зарезервированных IID http://www.iana.org/assignments/ipv6-interface-ids/ipv6-interface-ids.xhtml

| Префикс           | Назначение                                                   | IPv4 аналог                                   |
| ----------------- | ------------------------------------------------------------ | --------------------------------------------- |
| `::/128`          | **Unspecified** Может быть использован только в качестве источника при изучении хостом своего адреса. | `0.0.0.0`                                     |
| `::1/128`         | **Loopback**                                                 | `127.0.0.1`                                   |
| `::ffff:0:0/96`   | **IPv4-Mapped** [RFC4038](https://datatracker.ietf.org/doc/html/rfc4038) IPv4 внутри IPv6 адреса. Пример: `::ffff:192.168.2.2` |                                               |
| `64:ff9b::/96`    | IPv4/IPv6 трансляторы RFC6052                                |                                               |
| `fc00::/7`        | **Unique Local Address (ULA)** [RFC4193](https://datatracker.ietf.org/doc/html/rfc4193) Приватные адреса. | `10.0.0.0/8` `172.16.0.0/12` `192.168.0.0/16` |
| `fe80::/10`       | **Link-Local Addresses** Немаршрутизируемы диапизон.         | `169.254.0.0/16`                              |
| `2001::/32`       | **Teredo** Переходный механизм.                              |                                               |
| `2001:0002::/48`  | **Benchmarking**                                             | `192.18.0.0/15`                               |
| `2001:0010::/28`  | **Orchid**                                                   |                                               |
| `2002::/16`       | **6to4** Переходный механизм.                                |                                               |
| `2001:db8::/32`   | **Documentation**                                            |                                               |
| `2000::/3`        | **Global Unicast**                                           |                                               |
| `ff00::/8`        | **Mulitcast**                                                | `224.0.0.0/4`                                 |
| `ff00::1`         | All nodes                                                    |                                               |
| `ff00::2`         | All routers                                                  |                                               |
| `ff00::1:FF00:xx` | Solicited Node                                               |                                               |
| `ff05::1:3`       | All DHCP Servers                                             |                                               |

![https://ptgmedia.pearsoncmg.com/images/chap4_9781587144776/elementLinks/04fig11_alt.jpg](ipv6.assets/04fig11_alt.jpg)

#### Multicast адрес

Нет смысла переписывать рфц, поэтому читай это https://www.rfc-editor.org/rfc/rfc4291#page-13

### **Типы по области рапространения**

* Интерфейс (Interface) - действителен только в пределах хоста.
* Глобальные (global uncast address) - в данный момент начинаются с **2** или **3**
* Локальные (unique local address) - приватные, глобальное не маршрутизируются, начинаются с **FD**
* Локальные канала связи (link-local address) - не маршрутизируются, используются в пределах одного сегмента сети (VLAN, коммутатор(ы)), начинаются с **FE80**, а остальные до идентификатора интерфейса нули
* Локальные адреса площадки (site local address) - не используются

## Особенности

* Заголовок IPv6  в два раза больше, чем IPv4 (40 vs 20 байт).
* Опциональная информация IPv4 переехала в IPv6 `Extension Headers`.
    * В IPv4 было отведено 40 байт под опции, в то время как в IPv6 опции могут занимать весь размер пакета, т.к. ходят в пейлоад.
* Автоматическая конфигурация адреса.
* ARP, ICMPv4 Router Discovery, ICMPv4 Redirect, Broadcast заменены на протокол `Neighbor Discovery (ND)`, который использует мультикаст для своих нужд.
* Пакеты не фрагментируются - PMTUD обязательный.
* Нет контроля контрольной суммы заголовка (этим занимаются вышестоящие протоколы).
* `Hop limit` то же, что и TTL.
* Все делается при помощи ICMPv6.
* Минимальный MTU для IPv6 1280байт.

### Сжимаем адрес

Все начальные нули можно убрать `0db8 --> db8`

Группы нулей можно записать как ноль `0000 --> 0`

Несколько групп нулей подряд можно заменить на двоеточие `:0:0: --> ::` (одна группа нолей подряд на доветочие не заменяется)

Для читабельности при сокращении подсетей, можно оставлять нулю соответственно длине префикса. Пример:

```bash
2001:0db8:0000:0000:0000:0000:0000:0000/64
# Можно так
2001:0db8::/64
# Но лучше вот так
2001:0db8:0:0::/64
```

### Считаем количество подсетей

Сколько в `/48` сети `/56` подсетей: 2^(56-48)=256

```bash
/32 2001:1111::/32
/48 2001:1111:2200::/48
/52 2001:1111:2233::/52
/56 2001:1111:2233:4400::/56
/64 2001:1111:2233:4455::/64
```



## Переходные механизмы

https://en.wikipedia.org/wiki/IPv6_transition_mechanism

### SIIT

*Stateless IP/ICMP Translation*

### 6in4

* Инкапсуляция IPv6 в IPv4 пакеты.
* Работы через тоннельного брокера.

### 6RD

**RD** - *Rapid Deployment* 

* Методу туннелирования IPv6 поверх IPv4 сети.
* IPv6 кодируется в IPv4.
* Необходима сигнализация.
* Необходима поддержка на CPE.

**DS-Lite**

* В сети оператора используется только IPv6 адресация.
* IPv4 трафик абонента инкапусулируется в IPv6 пакеты и доставляется до CG-NAT устройства.

### NAT64

* В сети оператора и у клиентов используется только IPv6.
* Весь трафик в IPv4 сеть транслируется на границе сети по особому принципу.
* Для трансляции испольузется специальный префикс `64::ff9b::/96`.
* Нужен `DNS64`, чтобы генерировать `AAAA` записи для IPv4 доменов.

Варианты:

* Stateless NAT64 (MAP-E/MAP-t ??)
* Statefull NAT64

### 464XLAT

* Устройства, который не поддерживают IPv4, транслируются в IPv6.
* Пакеты до IPv4 ресурсов на границе сети из IPv6 транслируются обратно в IPv4.

## ICMPv6

Типы сообщений

1. Сообщение об ошибке 
   1. Destination Unreachable - использует расширенный формат сообщения.
   2. Packet To Big - использует основной формат сообщения.
   3. Time Exceeded - использует расширенный формат сообщения.
   4. Parameter Problem - использует основной формат сообщения.
2. Информационное сообщение
   1. Echo Request
   2. Echo Reply
   3. Neighbor Discovery Protocol (NDP) - включает в себя несколько типов информационных сообщений
   4. Mulitcast Listener Discovery (MLD) - включает в себя несколько типов информационных сообщений

Общий формат сообщения

![image-20220107203148498](ipv6.assets/image-20220107203148498.png)

Расширенный формат добавляет в конец сообщения расширенную структуру с доп. информацией, а также поле `length`, содержащее длину оригинальной датаграммы, вызвавшей ошибку.

![image-20220107205529811](ipv6.assets/image-20220107205529811.png)

### Сообщения об ошибке

| **Type**                                              | **Code**                                                     |
| ----------------------------------------------------- | ------------------------------------------------------------ |
| **Destination Unreachable (1)**                       | (0) - No route to destination(1) - Communication with destination administratively prohibited (2) - Beyond scope of source address  (3) - Address Unreachable (4) - Port Unreachable (5) - Source address failed ingress/egress policy (6) - Reject route to destination (7) - Error in Source Routing Header |
| **Packet Too Big (2)** Parameter = next hop MTU       | (0) - Packet Too Big                                         |
| **Time Exceeded (3)**                                 | (0) - Hop Limit Exceeded in Transit (1) - Fragment Reassembly Time Exceeded |
| **Parameter Problem (4)** Parameter = offset to error | (0) - Erroneous Header Field Encountered (1) - Unrecognized Next Header Type (2) - Unrecognized IPv6 Option (3) - IPv6 First Fragment has incomplete IPv6 Header Chain |

Все коды и другие параметры ICMPv6 https://www.iana.org/assignments/icmpv6-parameters/icmpv6-parameters.xhtml

## Neighbor Discovery Protocol

Пять типов сообщений:

* [**RS**] Router Solicitation - 1-ый этап после подключения устройства, поиск маршрутиазтора.
    * `Dst` мультикаст адрес `ff02:;2` - все маршрутиазторы на линке.
    * `Src` адрес при этом Unspecified `::` или Link-local адрес интерфейса `fe80::xx:xx:xx:x`.
* [**RA**] Router Advertisement - 2-ой этап, маршрутизатор отвечает на RS.
    * `Dst` - `ff02::1` - все узлы на линке или юникаст адрес хоста, который послал RS `2001:db8::...`.
    * `Src` - Link-local адрес интерфейса.
    * Содержит в себе информацию
        * `Link prefixes`
        * `Specific routes`
        * `MTU`
        * `Use SLAAC`?
    * Так же рассылается периодически как RA Unsolicited на адрес `all hosts`.
* [**NS**] Neighbor Solicitation - сообщения для изучения соседей (L2 Address Resolution - unicast). Также используется в DAD.
* [**NA**] Neighbor Advertisement - ответ на NS, либо для распространения новой информации (unsolicited NA).
* [**Re**] Redirect - сообщает о более предпочтительном маршрутизаторе в сети.

RS, RA, NS, NA используют ICMPv6.

## MLD

Служит для двух вещей:

* Изучение хостов, готовых получать мультикаст.
* В каких мультикаст адресах хосты заинтересованы.

## SLAAC

Хост может автоматически сконфигурировать свой адреса, используя MAC интерфейса.

Этапы:

1. Сгенерировать Link-local адрес:
     * Используется Link-local префикс `fe80::/10` и модифицированный EUI-64 (Interface ID).
     * MAC интерфейса (48 бит) расширяется до 64 бит путем вставки байт `ff:fe` в середину.
     * 7-ой бит инвертируется - определяем является ID универсальным или локально администрируемым.
     * Проверка Duplicate Address Detection (DAD) при помощи Neighbor Discovery - занимает около 5 секунд.
2. Получить один или более Global-unicast адресов.
     * Отправка RS хостом.
     * Отправка RA маршрутизатором: Router address, Link prefix, SLAAC Allowed.
     * Генерируется Global-unicast адрес из префикса сети и EUI-64.
3. Исключить дублирование адреса на линке при помощи Duplicate Address Detection механизма.
     * DAD - 1 секунда.

### DAD

1. Отправка NB в линк. `Src` - unspecified. `Dst` - Solicited Node.
2. Если адрес используется, то использующий его хост посылает NA.
3. Хост генерирует новый адрес и вновь посылает NB.
4. Если хост не получил в ответ NA, то сам посылает NA на все хосты линка.
5. Хост назначает адрес на интерфейс.

## DHCPv6

В роли сервера может выступать как сам маршрутизатор, так и выделенный сервер.

Флаги RA:

* \***M** - *Managed Address Configuration* - 1 - сообщает, что адрес и доп. параметры могут быть назначены DHCPv6 сервером.
*  \***O** - *Other Configuration* - 1 - сообщает, что DHCPv6 сервер может выдать только доп. параметры, но адрес должен быть сконфигурирован автоматически (SLAAC).
* **L** - *On-Link* -1 - сообщает, что все хосты указанного префикса находятся на одном линке. 0 - сообщает, что если не удалось достучаться до какого-либо хоста напрямую, то нужно обращаться к маршрутизатору.
* **A** - *Address Configuartion* - 1 - сообщает, что хост должен сконфигурировать адрес автоматически (SLAAC). 0 - не используй SLAAC.

Таким образом, чтобы хост использовал только DHCPv6, флаги должны быть следующие `M=1` , `A=0`.

DHCPv6 делегирует префикс - Prefix Delegation (PD).

## RIPE

Порядок выделения IPv6 в RIPE. У других RIR порядок может быть иной.

* В данный момент для RIPE выделен диапазон `2a00:0000::/12`.
* RIPE раздает LIR-ам /32 или большие блоки (**inet6num ALLOCATED-BY-RIR**).
* RIPE раздает напрямую пользователям /48 блоки.
* Domain объекты можно заводить для префиксов через каждые 4 бита, начиная с /32 (/36, /40, /44, /44 ...)
* Provider Aggregatable (PA) - блоки, которые RIPE выделяет LIR. Могут в свою очередь распределяться дальше LIR-ом (**inet6num ASSIGNED**). Адреса принадлежат LIR, поэтому конечный пользователь при смене LIR, должен сделать ренамберинг. Назначенные LIR-ом блоки агрегируются в больший блок (**inet6num AGGREGATED-BY-LIR**) с указанием размера агрегируемых блоков (assignment-size). LIR также может делать субраспределение (**inet6num ALLOCATED-BY-LIR**).
* Provider Independent (PI) - блоки (минимум /48), которые RIPE напрямую выдает конечным пользователям (не LIR-ам). Тем не менее запрос адресов должен производиться через LIR. Адреса не будут привязаны к LIR.

Типы в сравнении с IPv4![image-20210603121821178](ipv6.assets/image-20210603121821178.png)

Диаграмма распределения блоков![image-20210603145357981](ipv6.assets/image-20210603145357981.png)

* LIR не может раздавать блоки больше /48 без разрешения RIPE. Нужно послать запрос.

## Распределение

| Префикс | Подсеть | Количество |
| ------- | ------- | ---------- |
| /29     | /48     | 512K       |
| /32     | /48     | 65536      |
| /48     | /64     | 65536      |
| /56     | /32     | 16 777 216 |

* Используем /64 на каждую подсеть или VLAN.
* Количество хостов в /64 смотреть смысла нет - их бесконечно много.
* На каждую точку присутствия можно выделять по несколько /48 (RIPE должен разрешить это).
* На каждый сайт можно выделить /64 для адресов лупбеков.
* Пилим подсети по 4 бита, чтобы спасти свой мозг.

Пример:

* /64 - одно или несколько устройств, PtP линки (даже если используем только /127) или для лупбеков.
* /60 - один пользователь, чтобы разделить на подсети по комнатам, например.
* /56 - роутер, свич, большая домашняя сеть или клиент ШПД провайдера.
* /52 - небольшая сеть, роутер или свич.
* /48 - сеть компании, колокейшн клиент и т.д.
* /44 - на всех клиентов ШПД

## Безопасность

[Тут](../../sec/ipv6.md)

