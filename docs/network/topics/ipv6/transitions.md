---
title: Transitions
---

## Стандарты

* [RFC9313](https://www.rfc-editor.org/rfc/rfc9313.txt) - Pros and Cons of IPv6 Transition Technologies for IPv4-as-a-Service (IPv4aaS)
* [RFC8683](https://www.rfc-editor.org/rfc/rfc8683.txt) - Additional Deployment Guidelines for NAT64/464XLAT in Operator and Enterprise Networks
* [RFC8215](ttps://www.rfc-editor.org/rfc/rfc8215.txt) - Local-Use IPv4/IPv6 Translation Prefix.
  * IPv6 prefix 64:ff9b:1::/48 for local use within domains that enable IPv4/IPv6 translation mechanisms.

## Термины

* **NSP** - NAT64 Network Specific Prefix.
* **WKP** - NAT64 Well-Known Prefix - используется, если NSP не указан.
* **PLAT** - Provider-Side Translator - используется для трансляции dst.
* **CLAT** - Customer-Side Translator - используется для трансляции src.
* **SIIT (EAMT)** - 


## NAT64/DNS64

## XLAT464

## MAP-T/MAP-E
