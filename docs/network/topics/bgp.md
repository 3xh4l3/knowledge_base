---
title: BGP
---

### Оптимизация

- `nei <> soft-reconfiguration inbound`

  Хранит копию полученных маршрутов в памяти, а не запрашивает у соседа. Не рвет TCP сесссию.

  Расходует память маршрутизатора.

  *Outbound*	работает по-умолчанию и использует _​​ADJ-RIB-OUT_ (доп. память не потребляется)

  ```
  clear ip bgp <> soft in
  ```

- `bgp route refresh`

  Просит соседа переслать всю таблицу _ADJ-RIB-OUT_ . Не рвет TCP сессию и не расходует память.

  ```
  clear ip bgp <> in
  ```

  Capability route refresh должно быть согласовано на этапе *OPEN*. Проверить совместимость можно командой:

  ```php
  show bgp nei 80.65.17.38
  ...
    Neighbor capabilities:
      Route refresh: advertised and received(new)
  ...
  ```

- `outbound route filtering`

  Позволяет послать соседу фильтр, на основе которого будут отправляться маршруты.

  Получатель ORF фильтра (тот кто отдает маршруты)

  ```
  neighbor 1.1.2.2 capability orf prefix-list receive
  ```

  Отправитель ORF фильтры (получатель маршрутов)

  ```
  neighbor 1.1.2.1 capability orf prefix-list send
  neighbor 1.1.2.1 prefix-list FILTER in
  ```

- `route dampening`

  Защита от распространения нестабильных маршрутов, полученных по eBGP

  ```
  router bgp 
    bgp dampening
  ```


* `tcp path-mtu-discovery`

  Позволяет протоколу BGP использовать TCP MSS больше, чем 536 байт, что ускоряет загрузку маршрутной информации.

### Стандартные параметры

- When **BGP** PMTUD is disabled, the **BGP Maximum Segment Size** (**MSS**) **defaults** to 536 as defined in RFC 879.



### Термины

* `ADJ-RIB-IN` - маршруты полученные в *UPDATE* в сыром виде до применения фильтров (`show ip bgp <nei> received-routes` - должен быть включен soft...inbound)

* `LOC-RIB` - лучшие маршруты после обработки алгоритмом выбора ( `show ip bgp` или `show ip bgp <nei> routes`)

* `ADJ-RIB-OUT` - (`show ip bgp <nei> advertised-routes`)

> 1. prefix received by multiple neighbors and stored in `adj-rib-in`
>
> 2. policy is consulted, and if policy allows, multiple entries for the same prefix could then stored in `loc-rib`
>
> 3. descision process runs, and picks only one path
>
> 4. one "best" path is passed to `adj-rib-out`
>
> 5. Best path is passed to routers routing table if no other entry is in the RT with a lower AD.
>
>    Step 5. is probably happening before step 4. but is not a requirement for step 4 to happen. Route doesn't need to be installed in the RIB in order to be propagated to be passed to the `adj-rib-out`.

* `NLRI` - Network Layer Reachability Information

* `RT` - Routing Table

`BGP EoR` - End-of-RIB - расширение BGP, улучшающее сходимость, применяемое в таких механизмах как Graceful Restart.

### Опции

`network <net> netmask <mask> ...` - маршрут должен точно совпадать с тем, что есть в таблице маршрутизации

`aggregate-address <net> <mask> [summary-only]` - создает агрегированный маршрут, если в *LOC-RIB* есть хотя бы один более длинный префикс. Разновидность суммаризации.

### BCP

- Все делаем через *route-map* кроме *ORF*, который настраивается исключительно через *prefix-list*

### Параметры

Коды атрибутов перечислены в документе https://www.iana.org/assignments/bgp-parameters/bgp-parameters.txt

Дампы BGP пакетов можно посмотреть у Джереми на сайте https://packetlife.net/captures/protocol/bgp/

### Best path selection

![bgp_path_selection](bgp.assets/bgp_path_selection.png)

## Софт

### Реализации

* [frrouting](https://frrouting.org/) - улучшенная версия quagga с поддержкой OSPF, RIP, IS-IS, PIM, LDP, BFD, Babel, PBR, OpenFabric, VRRP, EIGRP, NHRP
* [quagga](https://quagga.net/) - форк GNU Zebra, пакет ПО для маршрутизации - OSPF, RIP, BGP
* [yabgp](https://yabgp.readthedocs.io/en/latest/index.html#) - BGP на python, имеет встроенный REST API
* [gobgp](https://github.com/osrg/gobgp) - BGP на golang, поддерживает управление через gRPC, может работаеть как BMP server, а так же поддерживает различные спецификации  и расширения протокола BGP
* [exabgp](https://github.com/Exa-Networks/exabgp) - преобразователь BGP сообщений в текст или JSON



### Мониторинг