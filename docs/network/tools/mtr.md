---
title: MTR
---



Типовой запрос

```bash
mtr -w -t -b -T -r -y 2 -c 2 52.94.17.134

HOST: snort                                                  Loss%   Snt   Last   Avg  Best  Wrst StDev
  1. RU  206.249.226.109.ip.orionnet.ru (109.226.249.206)     0.0%     1    0.5   0.5   0.5   0.5   0.0
  2. RU  core2.orionnet.ru (94.73.254.129)                    0.0%     1    0.5   0.5   0.5   0.5   0.0
  3. RU  c0-mbr.cn.orionnet.ru (109.226.254.253)              0.0%     1    0.8   0.8   0.8   0.8   0.0
  4. RU  kyk02rb.transtelecom.net (188.43.241.46)             0.0%     1    0.4   0.4   0.4   0.4   0.0
  5. RU  frt02.transtelecom.net (217.150.60.226)              0.0%     1   76.4  76.4  76.4  76.4   0.0
       [MPLS: Lbl 26424 Exp 0 S 1 TTL 251]
  6. RU  Amazon-gw.transtelecom.net (217.150.60.225)          0.0%     1   75.8  75.8  75.8  75.8   0.0
  7. ??? ???                                                 100.0     1    0.0   0.0   0.0   0.0   0.0
  8. ??? ???                                                 100.0     1    0.0   0.0   0.0   0.0   0.0
  9. US  54.239.4.216                                         0.0%     1   76.4  76.4  76.4  76.4   0.0
 10. ??? ???                                                 100.0     1    0.0   0.0   0.0   0.0   0.0
 11. ??? ???                                                 100.0     1    0.0   0.0   0.0   0.0   0.0
 12. ??? ???                                                 100.0     1    0.0   0.0   0.0   0.0   0.0
 13. ??? ???                                                 100.0     1    0.0   0.0   0.0   0.0   0.0
 14. US  dynamodb.eu-central-1.amazonaws.com (52.94.17.134)   0.0%     1  172.4 172.4 172.4 172.4   0.0
```

```bash
-w - не обрезать длинные надписи (hostname)
-t - использовать curses если это возможно
-b - печатать IP адреса
-r - режим отчета
-с - количество циклов
-T - использовать TCP вместо ICMP (можно указать порт ключом -P)
-u - использовать UDP вместо ICMP
-y - 0:AS, 1:Prefix, 2:Country, 3:RIR, 4:Alloc date (можно менят в процессе нажатием y)
-e - показывать информацию MPLS
---
другие полезные опции:
-i - интервал между icmp запросами, если меньше 1 секунды, нужны root права
```

```bash
#!/bin/bash
# Script run MTR for each sended host 
# and save result to specified file.

RESULTFILE=$1

if [[ -z "$RESULTFILE" ]]; then
    echo "Usage: ./mtr_hosts.sh <filename>"
    exit 0
fi

> $RESULTFILE

echo "Enter hosts below in line by line."
echo "Press Ctrl+X for exit."
while read -r line; do 
    if [[ $line ]]; then
        echo -e "\nTrace $line" | tee -a $RESULTFILE 
        sudo mtr -w -t -b -y 0 -c 10 -r -i 0.2 $line >> $RESULTFILE
        echo -e "Done. Result saved to $RESULTFILE"
    else
        echo "You send an empty string"
    fi
done
```

