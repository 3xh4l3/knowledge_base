---
title: RIPE
---

# RIPE

Для экзамена:

* При создании **LIR** RIPE создает автоматически три объекта: `mntner`, `organisation` и 2 объекта `role`
* **LIR**  в праве распределять только ресурсы `inetnum` и `inet6num`.
* Атрибут `email` для объекта `role` обязателен.



![image-20210309230747890](ripe.assets/image-20210309230747890.png)