---
title: Маршруты
---

Типы маршрутов:

* **STATIC** - статические маршруты
* **GENERATED** - сгенерированные маршруты (суммарные)
* **AGGREGATE** - агрегированные маршруты (суммарные)

Все эти машруты настраиваются в **routing-options**, поэтому все примеры приводятся относительно этой секции.

**Preference** в понятиях Juniper это административная дистанция (чем меньше, тем лучше).

## STATIC

* ==next-hop== по-умолчанию должен быть *directly connected*. Если нет, то нужно указать опцию **resolve** для рекурсивного разрешения.
* ==next-hop== должен быть доступен (*reachable*)
* ==next-hop==-ов может быть несколько, но активен будет только один, если не настроен *load balancing*
* ==next-hop== может быть *qualified*, то есть исполюзующий какой-либо атрибут (preference, metric и т.д.).
* "Корзина" ==reject== - отбросить пакет и послать в ответ *icmp*. Маршрут всегда активен.
* "Корзина" ==discard== - молча отбросить пакет. Маршрут всегда активен.
* ==preference== 5

!!! question
    Каким образом выбирается *next-hop* при отсутствии load-balancing-а?

```c
// 172.16.1.2 - directly connected адрес
set static route 0.0.0.0/0 next-hop 172.16.1.2

// 2.2.2.2 - доступен через другой маршрут, включаем route resolution
set static route 0.0.0.0/0 next-hop 2.2.2.2
set static route 2.2.2.2/32 next-hop 172.16.1.2
set static route 0.0.0.0/0 resolve

// Несколько next-hop-ов
set static route 0/0 next-hop [172.16.1.1 172.16.2.1]

// Qualified next-hop (может использоваться одновременно с next-hop)
// в данном примере будет выбран маршрут с МЕНЬШИМ preference
set static route 0/0 next-hop 172.16.1.1
set static route 0/0 qualified-next-hop 172.16.1.1 preference 10

// Reject/Discard
set static route 192.168.0.0/16 reject
set static route 192.168.0.0/16 discard
```

## AGGREGATE

* Нельзя указать *next-hop*. Следовательно вс пакеты по этому маршруту отправляются в "корзину".
* ==reject== - поведение по-умолчанию, но можно задать **discard**.
* Агрегированный маршрут должен иметь хотя бы один более специфичный маршрут, чтобы стать активным.
* Один маршрут может быть участником только **одного** агрегированного маршрута (одник агрегированные маршруты могут входить в другие агрегированные маршруты).
* Какие маршруты буду "зажигать" агрегированный можно указать через *policy*.
* ==preference== 130

```c
// Меняем поведение по-умолчанию (reject)
set aggregate route 10.0.0/24 discard

// Указываем, какие маршруты будут входить в агрегированный
set aggregate route 172.16.100.0/26 policy contributing

top edit policy-options policy-statement contributing
set term 1 from route-filter 172.16.100.0/28 exact
set term 1 from route-filter 172.16.100.48/28 exact
set term 1 then accept
set term 2 then reject 
```

## GENERATED

* Все то же, что и в AGGREGATE, но ==next-hop== наследуюется из основного спецефичного маршрута, который выбирается на основе атрибутов.
* Использует так называемый ==gateway of last resort== для пересылки пакетов.
* В таблице машрутизации отображается как *aggregate* с *next-hop*-ом.
* **local** и **direct** маршруты не активируют сгенерированный маршрут, т.к. не имют *next-hop*.
* Не добавляет ==aggregator== атрибут в BGP анонсы, как и ==as-set==.

