---
title: "Заметки"
---

В маршрутах можно опускать нули:
```c
// Посмотреть
>show route 0/0
// Настроить
#set routing-options static route 0/0 next-hop ...
```

Использование встроенной помощи:
```c
[edit routing-options static route 0.0.0.0/0]
#help apropos next-hop
set next-hop 
    Next hop to destination
set qualified-next-hop 
    Next hop with qualifiers
set qualified-next-hop <nexthop> mac-address <mac-address> 
    Next-hop Mac Address
...
```

