---
title: Настройка
---

# Configuration

Настройка оборудования Juniper на примере MX80.

## NetFlow5

```json
#show configuration firewall family inet filter SAMPLE                  
interface-specific;
term default {
    then {
        sample;
        accept;
    }
}

#show configuration interfaces ae0.3931 
description "<< XXX-DS0 via XXX >>";
vlan-id 3931;
family inet {
    filter {
        input SAMPLE;
        output SAMPLE;
    }
    address 3.1.2.2/31;
}

#show configuration forwarding-options sampling 
sample-once;
input {
    rate 1;
    run-length 0;
}
family inet {
    output {
        flow-server 3.1.2.1 {
            port 9996;
            source-address 3.1.2.254;
            version 5;
        }
    }
}
```

