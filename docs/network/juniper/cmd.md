---
title: "Команды"
---

## Маршрутная информация

*Forwarding table*

```c
show route forwarding-table destination 0/0 ...
```

*BGP received/advertised*

```c
show route receive-protocol bgp <nei> ...
show route advertising-protocol bgp <nei> ...
```

*Неактивные агрегированные/сгенерированные маршруты*

```c
show route protocol aggregate hidden
```

*Маршруты, зажигающие агрегированный/сгенерированный маршрут, можно увидеть в расширенном выводе*

```c
show route 172.16/16 protocol aggregate extensive
```

