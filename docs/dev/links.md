---
title: "Ссылки"
---

# Ссылки

## Программирование для детей

* https://code.org/ - по типу Scratch с объяснениями и программой обучения, в том числе и для самых маленьких. Бесплатно.
* https://scratch.mit.edu/ - ну собственно Scratch
* https://education.minecraft.net/trainings/code-builder-for-minecraft-education-edition/ - пишем модификации для майнкрафт, на Scratch  в том числе.
* https://blockly.games/ - для самых маленьких. Библиотека компании Google.

