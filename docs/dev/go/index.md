---
title: "Golang"
---

## Ссылки

* https://go.dev/learn/ - основной ресурс для вкатывания
* https://gobyexample.com/ - Go на примерах, очень полезно
* https://github.com/avelino/awesome-go - awesomelist Go
* http://golang-book.ru/ - введение на русском
* https://www.golang-book.com/ - оригинал
* https://github.com/dariubs/GoBooks - сборник лучших книг