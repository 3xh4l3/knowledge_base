---
title: Tools
---

# Тулы



## Окружение

* [pyenv](https://github.com/pyenv/pyenv) - менеджер виртуальных окружений и версии python
* [Poetry](https://python-poetry.org/) - резолвер зависимостей, деплой проектов, запуск тестов, создание изолированного окружения
* Pipenv
* Conda
* pip-tools
* virtualenvwrapper
* pyenv-virtualenv

**Сводная таблица**

<img src="tools.assets/image-20201117123019716.png" alt="image-20201117123019716" style="zoom:67%;" />

## Линтеры

[Flake8](https://flake8.pycqa.org/en/latest/) - автоматический форматер в соответствии с PEP8

[Black](https://black.readthedocs.io/en/stable/) - еще один автоматический форматер кода 

[Bandit](https://bandit.readthedocs.io/en/latest/) - инструмент для поиска потенциальных уязвимостей в коде