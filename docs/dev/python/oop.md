---
title: 'ООП'
---

# ООП

## Dunder методы

==**Double Under (Underscores)== - магические методы, использующиеся для перегрузки операторов.

* `__getaatribute__` - выполняется перед поиском атрибута. :heavy_exclamation_mark: Можно легко получить бесконечную рекурсию. Допустим, если внутри метода используется функция `getattr`.
* `__getattr__` - выполняется, если атрибут не был найден.

## Ленивые атрибуты

Если нам нужен атрибут, который не будет инициализироваться при инстанцировании класса, то можно объявить его как метод с декоратором `@property`

```python
class Foo():
    
    @property
    def bar(self):
        return 'Some value'
f = Foor()
print(f.bar)
#Some value
```

Но здесь нужно учитывать, что при каждом обращении к атрибуту будет вызываться метод `bar`, поэтому атрибуты, требующие тяжелых операций следует кешировать. Для этого удобно пользоваться декоратором `cached_property` из библиотеки `functools`.

```python
from functools import cached_property

class Foo():
    
    @cached_property
    def bar(self):
        return 'Some value'
f = Foor()
print(f.bar)
#Some value
```



Можно создать readonly атрибут:

```python
class Foo():
	def __init__(self, atr):
		self._atr = atr
	
    @property
    def atr(self):
        return self._atr
```

Единственный способ изменить атрибут `atr` это перезаписать атрибут `_atr`.

## Метод класса

Метод, который принимает на вход сам класс и какие-то аргументы. Возвращает класс.

```python
def class_decor(cls):
    cls.some_attr = 'Hello!'
    return cls

@class_decor
class foo()
	def __init__(self):
        pass
    
foo.some_attr
# Hello!
```



## Статический метод

Метод класса, который не зависит от инстанса, и зависит только от аргументов.

## Декоратор класса

@dataclass

## Дескрипторы

Любой объект, у которого есть методы `__set__`, `__get__` или `__delete__`. Позволяет вклиниться в стандартный процесс создания объектов и их атрибутов.

```python
# Дескриптор
class Descr():
    def __get__(self, instance, cls):
        pass
    
    def __set__(self, instance, value)
    	pass
    
class Foo():
    bar = Descr()
```



Например декоратор `@property` реализует дескриптор. 

## Магические методы и атрибуты

`__slots__` - определить атрибуты объекты. Другие создавать нельзя. Атрибуты будут записаны не в `__dict__`, а в кортеж. Новые атрибуты создавать нельзя. Используется для оптимизации памяти.

