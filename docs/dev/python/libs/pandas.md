---
title: Pandas
---

# Pandas

## Примеры

### Подсчет

```python
import pandas as pd 
  
df = pd.DataFrame({'A': ['foo', 'bar', 'g2g', 'g2g', 'g2g', 
                                'bar', 'bar', 'foo', 'bar'], 
                   'B': ['a', 'b', 'a', 'b', 'b', 'b', 'a', 'a', 'b'] }) 
  
# Первый способ
count = df.groupby(['A']).count() 
print(count) 

# Второй способ
count = df['A'].value_counts() 
print(count) 
```