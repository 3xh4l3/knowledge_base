---
title; PyNetbox
---

# PyNetbox

Создание кабеля для подключения чего либо

```python
nb.dcim.cables.create(
    termination_a_type='dcim.interface',
    termination_a_id=<id интерфейса>,
    termination_b_type='dcim.interface',
    termination_b_id=<id интерфейса>
)
```

Типы подключений

```python
circuits.circuittermination
dcim.consoleport
dcim.consoleserverport
dcim.frontport
dcim.interface
dcim.powerfeed
dcim.poweroutlet
dcim.powerport
dcim.rearport
```

