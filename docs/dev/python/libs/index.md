---
title: Главная
---

# Библиотеки

Здесь собраны библиотеки, которые я часто использую в своих скриптах

## Работа с данными

* https://jmespath.org/ - библиотека для работы с данными в JSON формате. 

## Парсеры

* https://pypi.org/project/ttp/ - Template Text Parser.
  * https://dmulyalin.github.io/ttp_templates/ - колекция парсеров для TTP
* https://pypi.org/project/pynumparser/ - работы с последовательностями чисел. Например удобно разворачивать строки вида `1,4-7,9 ` в список `[1,4,5,6,7,9]` и наоборот. Эт

## Линтеры и улучшайзеры кода

* **isort** - сортировщий импортов.

* **black**

* https://github.com/mintlify/vscode-docs - удивительная штука, которая пишет докстринги в коде за нас с вполне осознанными комментариями

  

## Сетевое

* https://netutils.readthedocs.io/en/latest/index.html - есть преобразователь вланов с писок и обратно =(^.*.^)=
* https://github.com/thatmattlove/oui - работа с MAC адресами.
* https://suzieq.readthedocs.io/en/latest/ - платформа наблюдения за сетью.



## **Интерфейсы**

* https://github.com/Textualize/rich-cli - подсветка синтаксиса и чтение маркдаунов в консоли

