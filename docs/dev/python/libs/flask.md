---
title: Flask
---

# Flask

Простое приложение `app.py`

```python
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello():
    return 'Приуэ!'
```

Запускаем приложение

```bash
export FLASK_APP=app.py
# Чтобы тестовый сервер слушал не только локалхост
flask run -h 0.0.0.0
```

## Debug

Включить `debug` режим

```python
from flask import Flask
app = Flask(__name__)

# Так
app.debug = True

# Или так
if __name__ == "__main__":
    app.run(debug=True)
```

