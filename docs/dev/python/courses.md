---
title: Курсы
---

# Python

## Видео

* Test Automation University - [Тестирование при помощи библиотеки Pytest](https://testautomationu.applitools.com/pytest-tutorial/ ) 



## Текст

* RealPython - [Python Web Applications with Flask](https://realpython.com/python-web-applications-with-flask-part-i/) 
* Miguel Grinberg - [The Flask Mega tutorial](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world)
* 