---
title: "Сетевое"
---

# Сетевоe

## Библиотеки

## Tips and tricks

Конвертируем питоновский список в строку-список (`switchport`) вланов для cisco и другого оборудования:

```python
def vlan_list_to_string(vlans: list) -> str:
    """ Conevert list of VIDs to Cisco VLAN
   switchport string.
   Example: [2,10,13,14,15,16,17,18,19,20,3000]
   Result:  '2,10,13-20,3000'
    """
    vlans = sorted(set(vlans))

    st= ''
    l = len(vlans) - 1

    for i, v in enumerate(vlans):
        if i < l:
            if (v == vlans[i + 1] - 1):
                if i > 0:
                    if st[-1:][0] != '-':
                        st += str(v) + '-'
                else:
                    st += str(v) + '-'
            else:
                st += str(v) + ','
        else:
            st += str(v) + ','
    return st[:-1]
```

Конвертируем `switchport` строку  в список:

```python
def vlan_string_to_list(string: str) -> set:
    """ Convert switchport string to set of VIDs
    Example: '2-5,10,12-13'
    Result: (2,3,4,5,10,12,13)
    """
    row = []
    
    for v in str.split(','):
        sp = v.split('-')
        if len(sp) > 1:
            row += range(int(sp[0]),int(sp[1]) + 1)
        else:
            row.append(int(sp[0]))

    return set(sorted(row))
```



