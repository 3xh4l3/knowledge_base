---
title: Templates
---

# Шаблоны

## Консольное приложение

Вариации шаблонов консольных приложений, то есть скриптов, принимающих различные аргументы.

### argparse

```python
#!/usr/bin/env python3
import argparse
import pathlib

WDIR = pathlib.Path(__file__).parent.absolute()

def main():
    """
    """
    _, args = get_args()

def get_args():
    """ Парсер аргументов
    """
    parser = argparse.ArgumentParser(prog="PROGNAME")

    # Пример аргумента
    parser.add_argument(
        "-e",
        "--example",
        metavar="<show in help>",
        action='store_const',
        const="default_value",
        help="Example arguments"
    )

    return parser, parser.parse_args()


if __name__ == "__main__":
    main()

```

## Пути

Не путю.

**Python3**

```python
import pathlib
# До запускаемого скрипта
pathlib.Path(__file__).parent.absolute()
# Текущая директория
pathlib.Path().absolute()
```

**Python2+3**

```python
import os
# До запускаемого скрипта
os.path.dirname(os.path.abspath(__file__))
# Текущая директория
os.path.abspath(os.getcwd())
```

