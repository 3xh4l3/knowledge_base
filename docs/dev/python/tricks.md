---
title: "Хитрости"
---

## Ссылки

* https://dbader.org/python-tricks - от автора книги Python tricks - Buffet of swesome python features

### Общие

Дзен python

```python
import this
```

### Условные операторы

Способы проверки флагов  
```python
x, y, z = 0, 1, 0

if x == 1 or y == 1 or z == 1:
    print('passed')

if 1 in (x, y, z):
    print('passed')

# Если истинно любой из
if x or y or z:
    print('passed')

if any((x, y, z)):
    print('passed')
```

### Словари

Способ смержить словари (python 3.5+)
```python
>>> x = {'a': 1, 'b': 2}
>>> y = {'b': 3, 'c': 4}

>>> z = {**x, **y}

>>> z
{'c': 4, 'a': 1, 'b': 3}

# Вариант для python2
>>> z = dict(x, **y)
>>> z
{'a': 1, 'c': 4, 'b': 3}

# В этом примере, python мержит ключ словарей 
# в порядке, перечисленном в выражении, перезаписывая
# дубликаты с лева на право.
```

Сортировка словаря по значениям
```python
>>> xs = {'a': 4, 'b': 3, 'c': 2, 'd': 1}

>>> sorted(xs.items(), key=lambda x: x[1])
[('d', 1), ('c', 2), ('b', 3), ('a', 4)]

# Или

>>> import operator
>>> sorted(xs.items(), key=operator.itemgetter(1))
[('d', 1), ('c', 2), ('b', 3), ('a', 4)]
```

Использование метода `get` и его аргумента `default`
```python
name_for_userid = {
    382: "Alice",
    590: "Bob",
    951: "Dilbert",
}

def greeting(userid):
    return "Hi %s!" % name_for_userid.get(userid, "there")

>>> greeting(382)
"Hi Alice!"

>>> greeting(333333)
"Hi there!"
```

### Классы

Создаем класс легко при помощи типа `namedtuple`
```python
>>> from collections import namedtuple
>>> Car = namedtuple('Car', 'color mileage')

# Класс работает как обычно
>>> my_car = Car('red', 3812.4)
>>> my_car.color
'red'
>>> my_car.mileage
3812.4

# Бонусом получаем строкое представление
>>> my_car
Car(color='red' , mileage=3812.4)

# Как и кортежи, именованные кортежи неизменяемы
>>> my_car.color = 'blue'
AttributeError: "can't set attribute"
```

### Регулярки

Если парсить регуляркой с именованными группировками, то автоматически создаться словарь для соответствующих групп

```python
import re

s = 'Interface: igb0  ---> mac: 01:02:03:af:ac:ad'

m = re.match(r"^Interface: (?P<interface>\S+).*mac: (?P<mac>\S+)", s)
m.groupdict()
>> {'interface': 'igb0', 'mac': '01:02:03:af:ac:ad'}
```

### Модули и пути

Чтобы нормально импортировтаь модули, из вышестоящией директории

```python
from pathlib import Path
from sys import path

path.insert(1, str(Path(__file__).parent.parent.absolute()))
import something # ../../something.py
```



