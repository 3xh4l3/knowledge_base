---
title: GIT
---

# GIT

## Настройка

```bash
git config --global user.name "Ivan Ivanov"
git config --global user.email "ii@mail.ru"
git config --global pull.rebase false
```



## Хитрости

**Добавить пустую папку в репозиторий.**

Создаем в папке файл `.gitignore` со следующим содержимым

```bash
*
*/
!.gitignore
```

## Команды

```bash
# Создать ветку и переключиться на нее
git checkout -b <branchname>
# Загрузить ветку в репу
git push <remote name> <branchname>
# Сделать апстримом нужную ветку и загрузить изменения
git push --set-upstream <remote name> <branchname>
# Посмотреть все ветки
git branch -a
```

