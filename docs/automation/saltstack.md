---
title: Saltstack
---

# Saltstack

# Saltstack

## Установка

Master

```bash
curl -L https://bootstrap.saltstack.com -o install_salt.sh
sudo sh install_salt.sh -M -x python3
```

Minion

```bash
curl -L https://bootstrap.saltstack.com -o install_salt.sh
sudo sh install_salt.sh
```

Нужные ключики

```bash
# Minion
-A  Pass the salt-master DNS name or IP. This will be stored under
        ${BS_SALT_ETC_DIR}/minion.d/99-master-address.conf
```

### Debian 10

На 10-ке salt будет работать только с поддержкой python3

```
wget -qO - https://repo.saltstack.com/py3/debian/10/amd64/latest/SALTSTACK-GPG-KEY.pub | sudo apt-key add -
echo "deb http://repo.saltstack.com/py3/debian/10/amd64/latest buster main" >> /etc/apt/sources.list.d/saltstack.list
apt install salt-master salt-minion salt-ssh salt-syndic salt-cloud salt-api
```

Мастер по-умолчанию слушает порты

```bash
tcp    LISTEN     0      128       *:4505 - На этот порт долбятся миньоны за новыми заданиями
tcp    LISTEN     0      128       *:4506 - На этот порт миньоны шлют свои данные (ответы)
```

## Использование

### Настройка

Все доступные настройки мастера находятся в файле `/etc/salt/master`

Папка с pillars

```yaml
pillar_roots:
  base:
    - /srv/pillar
```

Но основной файл лучше не править в виду его большого размера, а все настройки производить в соответствующих директориях, добавив в них файл с расширением `.conf`

```bash
# Пример
/etc/salt/master.d/master.conf
/etc/salt/minion.d/minion.conf
/etc/salt/proxy.d/proxy.conf
...
```

Пример **master.conf**

```yaml
state_verbose: False
state_output: mixed
file_roots:
  base:
    - /srv/salt
    - /srv/salt/states
    - /srv/salt/templates
pillar_roots:
  base:
    - /srv/salt/pillar
```

### Команды

Каждое соединение между миньоном и мастером шифруется ключом. После установки каждый миньон шлет мастеру свой публичный ключ, где он ждет того, чтобы его одобрили. Прежде чем слать команды на миньон с мастера, необходимо принять его ключ.

На мастере

```bash
# Показать версии компонентов
salt --versions-report
# Посмотреть все ключи
salt-key --list-all
salt-key -L
# Принять какой-то конкретный
salt-key --accept=<key>
# Принять все ключи
slat-key --accept-all
# Очистить кеш переменных
salt * saltutil.refresh_pillar
```

Отправка команд

```bash
# Проверить доступность миньонов
salt '*' test.ping
# Ввести команду удаленно
salt '*' cmd.run 'ls -l /etc'
# Таргетирование
salt 'minion1' disk.usage
salt 'minion*' disk.usage           # С использованием Globbing
salt -G 'os:Ubuntu' test.ping       # С использованием Grains
salt -E 'minion[0-9]' test.ping     # С использованием регулярки
salt -L 'minion1,minion2' test.ping # Списком
salt -C 'G@os:Ubuntu and minion* or S@192.168.50.*' test.ping # Комбинированный режим
# Применение определенного state
salt '*' state.apply statename
# Тестовый прогон без внесения изменений
salt '*' state.apply statename test=true
# Запуск highstate (применить в стейты)
slat '*' state.apply

# Еще полезные команды
salt 'target' cmd.run 'command'
salt 'target' service.get_enabled
salt 'target' pkg.install git
salt 'target' network.interfaces
salt 'target' disk.usage
salt 'target' sys.doc
salt 'target' pillar.items
salt 'target' grains.items
```

Полный список исполняемых модулей https://docs.saltstack.com/en/latest/ref/modules/all/

**Аргументы**

```bash
# Ограничено на количество миньонов, на которых будет выполнена команда
salt --batch-size 10
# Вывод данных в нужном формате
... --out=json|yaml
```

### salt-ssh

Нужен для серверов, где нет возможности установить `salt minion`.

Основной файл настроки `/etc/salt/roster`, но можно указать и другой файл.

```yaml
host1:
    host: 192.168.0.1
host2:
    host: host2.example.com
host3:
    host: foo.bar.baz.example.com
```

Есть возможность задать настройки по-умолчанию в настройках `master` :

```yaml
roster_defaults:
    user: salt
    passwd: ***
    sudo: True
```

На стороне сервера необходимо добавить пользователя **salt** и разрешить ему выполнять **sudo** без пароля.

**Команды:**

```bash
# Посмотреть список серверов, добавленных в roster
salt-ssh -H
# Принимаем ключ сервера при работе с новым сервером
slat-ssh -i ...
# Посмотреть тип ОС (в целом команды те же, что и для случая с minon-ом на целевом хосте)
salt-ssh 'host' grains.get 'os'
```

### Файлы и термины

Общий формат файлов в Salt - `SLS`. SLS файл это смесь данных их шаблонов (по-умолчанию YAML+Jinja).

- **Master** - машина, с которой управляются миньоны либо серверы, подключенные через salt-ssh

- **Minion (slave)** - процесс, работающий на управляемых машинах

- **States** - конечное состояние хоста. Не содержат каких-либо спец. конфигураций, относящихся к конкретному хосту.

- **Highstate** - процесс, при котором скачивают top.sls, ищут в нем себя и выполняют states, которые к ним относятся.

- **Top** - основной файл, связывающий стейты или переменные с каким-либо миньоном/хостом. 

  *Пример:* states/top.sls, который определяет, что на машине host, должны выполняться условия перечисленных состояни.

  ```yaml
  base:
    'host':
      - users 
      - ntp
      - packages
  ```

  В папке должны иметься файлы описания состояний users.sls, ntp.sls и packages.sls.

  Пример:* pillar/top.sls, который определяет, какие именно серверы ntp должны использоваться, а так же какие пользователи должны быть заведены в системе.

  ```yaml
  base:
    '*':
      - ntp
      - users
  ```

  В этом случае в папке pillar так же должны быть файлы ntp.sls и users.sls, в которых перечислены нужные значения.

  В общем:

  Файл `top.sls` может находится в папке `**/pillar/` и и нужен для связывания переменных с минионами.

  Так же `top.sls` может находиться в папке `**/salt/` и выполняет своего рода роль `inventory` файла.

- **Pillars** - переменные для одного или нескольких минионов/хостов.

- **Grains** - статичные параметры, полученные от оборудования.

## Автоматизация сети

Устанавливат `salt-proxy` не нужно apt install salt-proxy

Установки прокси миньона NAPALM (для версии солта с 2017.7.3 нужная версия напалма не ниже 2.0.0). Так же в нашем случае salt должен быть установлен с поддержкой python3 (-x python3)

```
pip3 install napalm
```

Настраиваем proxy на той же машине где и мастер `proxy`

```
master: 127.0.0.1
# Чтобы ключи прокси складывались в отдельную папку
pki_dir: /etc/salt/pki/proxy
multiprocessing: False
```

/srv/pillar/top.sls

```
base:
    'bk-dc1':
        - bk-dc1
```

Настройки прокси. Этот pillar файл задает исключительно настройки прокси миньона. И желательно так и оставить, но если очень хочется, можно записать в этот филик какие-нибудь переменные.

/srv/pillar/bk-dc1.sls 

```
proxy:
    proxytype: napalm
    driver: ios
    host: 10.226.251.9
    username: admin
    passwd: pass
    optional_args:
        secret: passwd
```

запускаем прокси миньона

```
 systemctl status salt-proxy@bk-dc1
```

смотрим логи в /var/log/salt/proxy

Далее необходимо добавить ключ миньона в salt-master

После этого можно слать команды на миньона.

```bash
salt 'bk.dc1' snmp.config
salt 'bk.dc1' ntp.peers
salt 'bk.dc1' net.interfaces
salt 'bk.dc1' users.config
# Проверка подключения
salt 'bk.dc1' napalm.alive
```



### Практика

```bash
salt 'host' net.load_config text='ntp server 172.17.17.1'
```



# Best practice

https://docs.saltstack.com/en/latest/topics/best_practices.html

### Структурирование файлов

Правильная структура директорий описывает функциональность стейтов или формул и помогает пользователю быстро понять, что к чему относится.

Например

```yaml
/srv/salt/mysql/files/
/srv/salt/mysql/client.sls
/srv/salt/mysql/map.jinja
/srv/salt/mysql/python.sls
/srv/salt/mysql/server.sls
```

Будет иметь вид в top.sls

```yaml
base:
  'web*':
    - mysql.client
    - mysql.python
  'db*':
    - mysql.server
```

