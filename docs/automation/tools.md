---
title: Тулзы
---

# Инструменты

## REST API

**Тестирование**

 - **Postman** - мощный инструмент для отладки API  
[Скачать](https://www.postman.com/downloads/)

## Графика

- **NeXt UI Toolkit** - с виду неплохая штука для отрисовки сети (React JS)  
[Туториал](https://developer.cisco.com/site/neXt/learn/)  
[Еще туториал](https://developer.cisco.com/codeexchange/github/repo/NeXt-UI/next-tutorials/)  
[Репозиторий проекта](https://github.com/NeXt-UI)  
[Демонстрация работы на YouTube](https://youtu.be/gBsUDu8aucs)  

- **VIS.JS** - JS библиотека, при помощи которой так же можно отрисовывать сети  
https://visjs.org/  

## Репозитории и проекты

- **Network to Code** - команда, которая активно разрабатывает и развивает решения для автоматизации сети. Именно они создали `ntc-templates`.
https://www.networktocode.com/  
https://github.com/networktocode - проект на github с кучей крутых реп

- **Awesome Network Automation** - ==список крутых вещей для автоматизации сети==  от команды *Network to Code*.  
https://github.com/networktocode/awesome-network-automation  
